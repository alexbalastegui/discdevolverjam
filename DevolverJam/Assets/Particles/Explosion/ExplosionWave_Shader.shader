﻿Shader "Unlit/ExplosionWave_Shader"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
		_ErodeGlow("ErodeGlow", Float) = 1
    }
    SubShader
    {
		Tags { "RenderType" = "Transparent" "Queue" = "Transparent" }
		Blend SrcAlpha OneMinusSrcAlpha
		Zwrite Off
		LOD 100

        Pass
        {
            CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			// make fog work
			#pragma multi_compile_fog

            #include "UnityCG.cginc"
			#include "Assets/Materials/myInclude.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float4 uv : TEXCOORD0;
				float4 color : COLOR;
				
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                UNITY_FOG_COORDS(1)
                float4 vertex : SV_POSITION;
				float4 color : COLOR;
				float erode : TEXCOORD1;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;

			float _ErodeGlow;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = TRANSFORM_TEX(v.uv.xy, _MainTex);
				o.color = v.color;
				o.erode = v.uv.z;
                UNITY_TRANSFER_FOG(o,o.vertex);
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
				float4 tex = tex2D(_MainTex, i.uv+float2(0, _Time.y));

				float erode = valueStep(tex.r, 2, i.erode);
				float erodeFringe = valueStep(tex.r, 2, i.erode-_ErodeGlow);

				float4 col = (erode*i.color + (erode - erodeFringe)*(5 * i.color));

				return col;
            }
            ENDCG
        }
    }
}
