﻿Shader "Unlit/ExplosionErosion_Shader"
{
    Properties
    {
		_Tex("Texture", 2D)="white"{}
		_Alpha("Alpha texture", 2D)="white"{}
		_Smoke1("Smoke color 1", Color)=(1, 0, 0, 1)
		_Smoke2("Smoke color 2", Color)=(0, 1, 0, 1)
		[HDR]
		_Fire("Fire color", Color)=(1, 1, 0, 1)
		_AlphaProgress("Alpha progress", Range(0, 1)) = 0
		_FireProgress("Fire progress", Range(0, 2)) = 0
    }
    SubShader
    {
        Tags { "RenderType"="Transparent" "Queue"="Transparent" }
		Blend SrcAlpha OneMinusSrcAlpha
		Zwrite Off
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            // make fog work
            #pragma multi_compile_fog

            #include "UnityCG.cginc"
			#include "Assets/Materials/myInclude.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float4 uv : TEXCOORD0;
				float4 color : COLOR;
				float4 custom : TEXCOORD1;
				
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                UNITY_FOG_COORDS(1)
                float4 vertex : SV_POSITION;
				float4 color : COLOR;
				float age : TEXCOORD1;
				float erode : TEXCOORD2;
				float fire : TEXCOORD3;
            };

            sampler2D _Tex;
            float4 _Tex_ST;
			sampler2D _Alpha;
			float4 _Alpha_ST;

			float _FireProgress;
			float _AlphaProgress;
			float4 _Smoke1;
			float4 _Smoke2;
			float4 _Fire;

			float fireWidth = .25f;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = v.uv.xy;
				o.color = v.color;
				o.age = v.uv.z;
				o.erode = v.custom.x;
				o.fire = v.custom.y;
                UNITY_TRANSFER_FOG(o,o.vertex);
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
				_FireProgress = i.fire*2;

				float4 tex = tex2D(_Tex, i.uv);

				float4 baseColor = lerp(_Smoke1, _Smoke2, smoothstep(0, .5f, tex.r));
				float4 fireColor = _Fire * max(_FireProgress/2, tex.g* valueStep(tex.g, 2, 1-pow(1-_FireProgress, 3)));

				baseColor += fireColor;
				
				_AlphaProgress = i.erode;
				float4 alpha = tex2D(_Alpha, i.uv);
				alpha *= valueStep(tex.b, 2, _AlphaProgress);

				return fixed4(baseColor.rgb, alpha.r);
            }
            ENDCG
        }
    }
}
