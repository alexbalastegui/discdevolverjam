﻿Shader "Unlit/Flash_Shader"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
		_Cols("Columns", Float)=2
		_Rows("Rows", Float)=2
    }
    SubShader
    {
		Tags { "RenderType" = "Transparent" "Queue" = "Transparent" }
		Blend SrcAlpha OneMinusSrcAlpha
		Zwrite Off
		LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            // make fog work
            #pragma multi_compile_fog

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float4 uv : TEXCOORD0;
				float4 color : COLOR;
				float4 custom : TEXCOORD1;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                UNITY_FOG_COORDS(1)
                float4 vertex : SV_POSITION;
				float age : TEXCOORD1;
				float variance : TEXCOORD2;
				float4 color : COLOR;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;
			float _Cols;
			float _Rows;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = v.uv.xy;
				o.color = v.color;
				o.age = v.uv.z;
				o.variance = v.uv.w;
                UNITY_TRANSFER_FOG(o,o.vertex);
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                // sample the texture
				float index = floor(_Time.y+1234*i.variance);
                fixed4 col = tex2D(_MainTex, i.uv*float2(  1/_Cols , 1/_Rows) + float2(1/_Cols*(index%(_Cols)), 1/_Rows*floor(index/_Rows)));
                // apply fog
                UNITY_APPLY_FOG(i.fogCoord, col);
                return fixed4(i.color.rgb, i.color.a*col.r);
            }
            ENDCG
        }
    }
}
