﻿Shader "Unlit/ParticleDot_Shader"
{
    Properties
    {
		[HDR]
		_Color("Color", Color)=(1, 0, 0, 1)
		_Rad("Radius", Float)=1
		_Density("Density", Range(-10, 1))=1
    }
    SubShader
    {
		Tags { "Queue" = "Transparent" "RenderType" = "Transparent" }
		Blend SrcAlpha One
		Cull Off
		ZWrite Off
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            // make fog work
            #pragma multi_compile_fog

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
				float4 vertexColor : COLOR;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                UNITY_FOG_COORDS(1)
                float4 vertex : SV_POSITION;
				float4 vertexColor : COLOR;
            };

			float4 _Color;
			float _Rad;
			float _Density;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = v.uv;
				o.vertexColor = v.vertexColor;
                UNITY_TRANSFER_FOG(o,o.vertex);
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
				_Rad*=2;

				float2 centeredUV = i.uv + float2(-0.5, -0.5);
				float circle = length(centeredUV*_Rad);

				circle = 1-smoothstep(min(1, _Density), 1, circle);



                return float4(_Color.rgb, circle*i.vertexColor.a);
            }
            ENDCG
        }
    }
}
