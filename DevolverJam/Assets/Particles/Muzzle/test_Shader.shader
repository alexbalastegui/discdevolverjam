﻿Shader "Unlit/channelModulation_Shader"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
		[HDR]
		_Color("Color1", Color)=(1, 0, 0, 0)
		[HDR]
		_Color2("Color2", Color)=(1, 0, 0, 0)
		_AlphaOffset("AlphaOffset", Vector)=(1, 0, 0, 0)
		_Tiling("Tiling", Float)=1
		_AlphaRotSpeed("AlphaRotSpeed", Float)=0
    }
    SubShader
    {
        Tags { "RenderType"="Transparent" "Queue"="Transparent" }
		Cull Off
		Blend SrcAlpha One
		ZWrite Off
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            // make fog work
            #pragma multi_compile_fog

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float4 uv : TEXCOORD0;
				float4 vertexColor : COLOR;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
				float age : TEXCOORD1;
				float custom1 : TEXCOORD2;
                UNITY_FOG_COORDS(1)
                float4 vertex : SV_POSITION;
				float4 vertexColor : COLOR;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;

			float4 _Color;
			float4 _Color2;
			float _Tiling;
			float2 _AlphaOffset;
			float _AlphaRotSpeed;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
				o.vertexColor = v.vertexColor;
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				o.age = v.uv.z;
				o.custom1 = v.uv.w;
                UNITY_TRANSFER_FOG(o,o.vertex);
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                fixed4 tex = tex2D(_MainTex, (i.uv-0.5f)*float2(_Tiling, _Tiling)+0.5f);
				float2 alphaUV = 0.5f+(i.uv - 0.5f)*((1-i.custom1.xx)*15);

				float rotAngle = _AlphaRotSpeed * _Time.y;
				float2x2 mat1=float2x2(cos(rotAngle), -sin(rotAngle), sin(rotAngle), cos(rotAngle));
				float2x2 mat2=float2x2(cos(-rotAngle), -sin(-rotAngle), sin(-rotAngle), cos(-rotAngle));
				float2 alphaRotatedUV1 = 0.5 + mul(mat1, alphaUV - 0.5);
				float2 alphaRotatedUV2 = 0.5 + mul(mat2, alphaUV - 0.5);

                fixed4 texAlpha1 = tex2D(_MainTex, alphaRotatedUV1);
                fixed4 texAlpha2 = tex2D(_MainTex, alphaRotatedUV2);
				
				
				fixed3 col = lerp(_Color2.rgb, _Color.rgb ,tex.g) + fixed3(1, 1, 1)*tex.r;
				float alpha = (tex.r + tex.g)*i.vertexColor.a*(1 - (texAlpha1.b*texAlpha2.b));
				
                return fixed4(col.rgb*i.vertexColor.rgb, ceil(alpha)*i.vertexColor.a);
            }
            ENDCG
        }
    }
}
