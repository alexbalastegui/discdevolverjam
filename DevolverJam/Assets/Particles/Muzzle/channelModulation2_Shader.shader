﻿Shader "Unlit/channelModulation_Shader"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
		[HDR]
		_Color("Color1", Color)=(1, 0, 0, 0)
		[HDR]
		_Color2("Color2", Color)=(1, 0, 0, 0)
		_AlphaOffset("AlphaOffset", Float)=1
    }
    SubShader
    {
        Tags { "RenderType"="Transparent" "Queue"="Transparent" }
		Cull Off
		Blend SrcAlpha One
		ZWrite Off
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            // make fog work
            #pragma multi_compile_fog

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
				float4 vertexColor : COLOR;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                UNITY_FOG_COORDS(1)
                float4 vertex : SV_POSITION;
				float4 vertexColor : COLOR;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;

			float4 _Color;
			float4 _Color2;
			float _AlphaOffset;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
				o.vertexColor = v.vertexColor;
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                UNITY_TRANSFER_FOG(o,o.vertex);
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                fixed4 tex = tex2D(_MainTex, i.uv);
				
				fixed3 col = lerp(_Color2.rgb, _Color.rgb ,tex.g) + fixed3(1, 1, 1)*tex.r;

				
                return fixed4(col.rgb*i.vertexColor.rgb, (tex.r+ tex.g)*i.vertexColor.a);
            }
            ENDCG
        }
    }
}
