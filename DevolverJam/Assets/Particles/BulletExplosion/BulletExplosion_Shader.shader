﻿Shader "Unlit/BulletExplosion_Shader"
{
    Properties
    {
		_Rad1("Radius 1", Range(0, 1))=1
		_Rad2("Radius 2", Range(0, 1))=0.1
		_Density("Density", Range(0, 1))=1
		[HDR]
		_Col1("Start color", Color)=(1, .55, 0, 1)
		[HDR]
		_Col2("End color", Color)=(1, 0, 0, 1)
    }
    SubShader
    {
		Tags { "RenderType" = "Transparent" "Queue" = "Transparent" }
		Cull Off
		Blend SrcAlpha One
		ZWrite Off
		LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            // make fog work
            #pragma multi_compile_fog

            #include "UnityCG.cginc"
			#include "include.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
				float4 normal : NORMAL;
				float4 color : COLOR;
                float4 uv : TEXCOORD0;

            };

            struct v2f
            {
				float4 vertex : SV_POSITION;
                float2 uv : TEXCOORD0;
				float age : TEXCOORD1;
				float4 color : COLOR;
				float colorModulation : TEXCOORD2;
            };

            sampler2D _Texture;
            float4 _Texture_ST;

			float _Rad1;
			float _Rad2;
			float _Density;
			float4 _Col1;
			float4 _Col2;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
				o.color = v.color;
				o.uv = v.uv.xy;
				o.age = v.uv.z;
				o.colorModulation = v.uv.w;
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {/*
                fixed4 tex = tex2D(_Texture, i.uv);

				float mask = lerp(tex.b+tex.g, tex.g, i.age);
				float4 col =  tex.g*lerp(_Col1, _Col2, i.age) + tex.b*_Col2;

				col *= mask;
				col.a *= i.color.a;*/

				_Rad2 = i.age;
				_Rad2 = 1 - pow(1 - _Rad2, 3);


				float radialMask1 = 1-smoothstep(_Density/2, .5, length( 1/_Rad1*(i.uv - 0.5f)));
				float radialMask2 = 1-smoothstep(_Density/2, .5, length(1/_Rad2*(i.uv - 0.5f)));
				float donutMask = radialMask1 - radialMask2;

				float t = i.colorModulation;
				
				float4 color = donutMask * lerp(_Col1, _Col2, t);

				return fixed4(color);;
            }
            ENDCG
        }
    }
}
