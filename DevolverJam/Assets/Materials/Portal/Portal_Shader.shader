﻿Shader "Unlit/Portal_Shader"
{
	Properties
	{
		[HDR]
		_Col1("Color1", Color) = (1, 1, 1, 1)
		[HDR]
		_Col2("Color2", Color) = (0, 0, 0, 1)

		_MainTex("MainTexture", 2D) = "black"{}
		_TexRot("MainTextureRotation", Float) = 0
		_DistortTex("UV Distortion Texture", 2D) = "white"{}
		_DistortionStrength("UV Distortion Strength", Range(0, 1)) = 0
		_DistortTexRot("DistortionTextureRotation", Float) = 0
		_InnerTex("InnerTexture", 2D) = "white"{}
		_GlowingBorder("GlowingBorderRad", Range(0, 1)) = 0
		_GlowingBorderGlow("GlowingBorderGlow", Float) = 1
		_GlowingBorderPow("GlowingBorderPow", Float) = 1
		_GlowingBorderNoise("GlowingBorderNoiseStrength", Float) = 0
		_ArtifactsGlow("ArtifactsGlow", Float) = 1
		_ArtifactsDensity("ArtifactsDensity", Range(0, 1)) = 1

    }
    SubShader
    {
        Tags { "RenderType"="Transparent" "Queue"="Transparent" }
		ZWrite Off
		Blend SrcColor OneMinusSrcAlpha
		Blend SrcAlpha OneMinusSrcAlpha
		Cull Off
		
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            // make fog work
            #pragma multi_compile_fog

            #include "UnityCG.cginc"
			#include "portal_include.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float4 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
				float2 noiseUV : TEXCOORD1;
				float2 alphaMaskUV : TEXCOORD2;
				float2 screenPos : TEXCOORD3;
				float depth : TEXCOORD4;
                UNITY_FOG_COORDS(1)
                float4 vertex : SV_POSITION;
            };

			sampler2D _NoiseTex;
			float4 _NoiseTex_ST;
			sampler2D _InnerTex;
			float4 _InnerTex_ST;

			sampler2D _MainTex;
			float4 _MainTex_ST;
			sampler2D _DistortTex;
			float4 _DistortTex_ST;


			float4 _Col1;
			float4 _Col2;

			float _GlowingBorder;
			float _GlowingBorderGlow;
			float _GlowingBorderPow;
			float _GlowingBorderNoise;
			float _ArtifactsGlow;
			float _ArtifactsDensity;
			float _TexRot;
			float _DistortTexRot;
			float _DistortionStrength;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = v.uv;
				o.noiseUV = TRANSFORM_TEX(v.uv, _NoiseTex);
				o.alphaMaskUV = TRANSFORM_TEX(v.uv, _InnerTex);
				o.screenPos = ComputeScreenPos(o.vertex);
				o.depth = -mul(UNITY_MATRIX_MV, v.vertex).z * _ProjectionParams.w;
                UNITY_TRANSFER_FOG(o,o.vertex);
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {

				float radialMask = 1 - round(length(i.uv - 0.5));
				float4 distortionTex = tex2D(_DistortTex, 0.5+uvRot(i.uv-0.5, _Time.y*_DistortTexRot));
				float4 distortionTex2 = tex2D(_DistortTex, i.uv);
				float4 distortionTex3 = tex2D(_DistortTex, 0.5 + float2(0, _Time.y*.2) + lerp(i.uv, i.uv*distortionTex2, _DistortionStrength) - 0.5);

				//border
				_GlowingBorder = 1 - _GlowingBorder;
				float border = radialMask*smoothstep(_GlowingBorder/2, 1-_GlowingBorder/2, length(i.uv - 0.5));
				border -= distortionTex3.r*(_GlowingBorderNoise);
				border = pow(border, _GlowingBorderPow);
				border = 1 - border;
				border = min(1, max(0, border));

				//border *= saturate(distortionTex3.r);

				//texture
				float2 distortedUV = 0.5 + uvRot(lerp(i.uv, distortionTex.rg, _DistortionStrength)*i.uv - 0.5, _TexRot*_Time.y);
				float2 distortedUV1 = 0.5 + uvRot(lerp(i.uv, -3*(distortionTex.rg), _DistortionStrength)*i.uv - 0.5, -_TexRot*_Time.y);
				float4 tex = tex2D(_MainTex, distortedUV);
				float4 tex_ = tex2D(_MainTex, distortedUV1);
				
				float artifacts = min(1, smoothstep(_ArtifactsDensity, 1, (tex.r*tex_.r*radialMask)+.97));

				float4 innerTex1 = tex2D(_InnerTex, i.alphaMaskUV + float2(0, -.1*_Time.y));
				float4 innerTex2 = tex2D(_InnerTex, .75f*i.alphaMaskUV + float2(.2*_Time.y, 0));

				artifacts += (innerTex1.g*innerTex2.g*5);

				//portal assembly
				float4 colorInterp = lerp(_Col1, _Col2, clamp(0, 1, artifacts+border));
				float colorBoost = 1+(max(0, border * _GlowingBorderGlow) + max(0, (artifacts-(artifacts*border)) * _ArtifactsGlow));

				//return fixed4((radialMask*max(artifacts, 0.1f))+border,0, 0, 1);
                return fixed4(colorInterp.rgb*colorBoost, (radialMask*max(artifacts, 0.7f)) + border);
            }
            ENDCG
        }
    }
}
