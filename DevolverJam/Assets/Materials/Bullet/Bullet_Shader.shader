﻿Shader "Unlit/Bullet_Shader"
{
    Properties
    {
		_Color1("Color 1", Color)=(1, 1, 1, 1)
		_Color2("Color 2", Color)=(1, 0, 0, 1)
		_BorderWidth("Border width", Range(0, 1))=.1
		_FlickerSpeed("Flicker speed", Float)=0
		_Stripes("Stripes", Float)=0
    }
    SubShader
    {
        Tags { "RenderType"="Transparent" "Queue"="Transparent+1" }
		
		Blend SrcAlpha OneMinusSrcAlpha
		ZWrite Off

        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            // make fog work
            #pragma multi_compile_fog

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                UNITY_FOG_COORDS(1)
                float4 vertex : SV_POSITION;
            };

			float _BorderWidth;
			float4 _Color1;
			float4 _Color2;

			//zigzag
			float _FlickerSpeed;

			//waves
			float _Stripes;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = v.uv;;
                UNITY_TRANSFER_FOG(o,o.vertex);
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
				float2 centeredUV = i.uv - 0.5f;

				float radialMask = 1-smoothstep(0.495f, 0.505f, length(centeredUV));
				float colorModulation = step(_BorderWidth/2, length(centeredUV));

				//zigzag
				colorModulation = lerp(1-colorModulation, colorModulation, step(0, sin(_Time.y*_FlickerSpeed)));

				//waves
				if (_Stripes != 0) {
					float distanceField = abs((length(centeredUV*_Stripes)) - _Time.y) % 1;
					distanceField = min(0.9, distanceField);
					colorModulation = colorModulation + (1 - colorModulation)*distanceField;
				}
				/*colorModulation = smoothstep(0, 1 / _Stripes, colorModulation);
				colorModulation = min(0.9f, colorModulation);*/

				float4 col = lerp(_Color1, _Color2, colorModulation);
				col.a *= radialMask;

				//return colorModulation.xxxx;
                return col;
            }
            ENDCG
        }
    }
}
