﻿Shader "Unlit/CircleShadow_Shader"
{
    Properties
    {
		[HDR]
		_Color("Color", Color)=(0, 0, 0, 1)
		_Density("Density", Range(0, 1))=1
		_Rad("Radius", Float)=1
    }
    SubShader
    {
        Tags { "RenderType"="Transparent" "Queue"="Transparent" }
		Blend SrcAlpha OneMinusSrcAlpha
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            // make fog work
            #pragma multi_compile_fog

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                UNITY_FOG_COORDS(1)
                float4 vertex : SV_POSITION;
            };

			float4 _Color;
			float _Density;
			float _Rad;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = v.uv;
                UNITY_TRANSFER_FOG(o,o.vertex);
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
				_Density /= 2;

				float2 centeredUV = i.uv - 0.5f;
				float radialMask = 1-smoothstep(_Density, 1-_Density, 1/_Rad*length(centeredUV));

                return fixed4(_Color.rgb, radialMask*_Color.a);
            }
            ENDCG
        }
    }
}
