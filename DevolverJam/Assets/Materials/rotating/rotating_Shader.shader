﻿Shader "Custom/rotating_Shader"
{
	Properties
	{
		_Color("Color", Color) = (1,1,1,1)
		_Tint("Tint", Color) = (1, 1, 1, 1)
		_MainTex("Albedo (RGB)", 2D) = "white" {}
		_Glossiness("Smoothness", Range(0,1)) = 0.5
		_Metallic("Metallic", Range(0,1)) = 0.0
		_Angle("Angle", Range(0, 360)) = 0.0
	}
		SubShader
		{
			Tags { "RenderType" = "Opaque" "RenderPipeline" = "UniversalRenderPipeline"}
			LOD 200

			CGPROGRAM
			// Physically based Standard lighting model, and enable shadows on all light types
			#pragma surface surf Standard fullforwardshadows

			// Use shader model 3.0 target, to get nicer looking lighting
			#pragma target 3.0

			sampler2D _MainTex;

			struct Input
			{
				float2 uv_MainTex;
			};

			half _Glossiness;
			half _Metallic;
			fixed4 _Color;
			fixed4 _Tint;
			float _Angle;

			// Add instancing support for this shader. You need to check 'Enable Instancing' on materials that use the shader.
			// See https://docs.unity3d.com/Manual/GPUInstancing.html for more information about instancing.
			// #pragma instancing_options assumeuniformscaling
			UNITY_INSTANCING_BUFFER_START(Props)
				// put more per-instance properties here
			UNITY_INSTANCING_BUFFER_END(Props)

			void surf(Input IN, inout SurfaceOutputStandard o)
			{
				float2 myUVs;
				_Angle = (_Angle * 2 * 3.141592) / 360;
				myUVs.x = (IN.uv_MainTex.x - 0.5f)*cos(_Angle) - (IN.uv_MainTex.y - 0.5f)*sin(_Angle) + 0.5f;
				myUVs.y = (IN.uv_MainTex.x - 0.5f)*sin(_Angle) + (IN.uv_MainTex.y - 0.5f)*cos(_Angle) + 0.5f;

				// Albedo comes from a texture tinted by color
				fixed4 c = tex2D(_MainTex, myUVs) * _Color * _Tint;
				o.Albedo = c.rgb;
				// Metallic and smoothness come from slider variables
				o.Metallic = _Metallic;
				o.Smoothness = _Glossiness;
				o.Alpha = c.a;
			}
			ENDCG
		}
			FallBack "Diffuse"
}
