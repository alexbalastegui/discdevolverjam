﻿Shader "Unlit/bossFace_shader"
{
    Properties
    {
        _FaceTexture ("Face texture", 2D) = "white" {}
		_Cols("Columns", Float)=4
		_Index("Index", Float)=0
		_Speed("Speed", Float)=1
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            // make fog work
            #pragma multi_compile_fog

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                UNITY_FOG_COORDS(1)
                float4 vertex : SV_POSITION;
            };

			sampler2D _FaceTexture;
			float4 _FaceTexture_ST;

			float _Cols;
			float _Index;
			float _Speed;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = v.uv;
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
				_Index = _Index + _Time.y*_Speed;
                // sample the texture
				float2 flipBookUV = i.uv*float2(1 / _Cols, 1);
				flipBookUV.x += ((floor(_Index)%_Cols)*(1/_Cols));
                fixed4 col = tex2D(_FaceTexture, flipBookUV);
                return col;
            }
            ENDCG
        }
    }
}
