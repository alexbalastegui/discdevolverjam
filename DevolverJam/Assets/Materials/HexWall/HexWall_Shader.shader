﻿Shader "Unlit/HexWall_Shader"
{
    Properties
    {
		_PulseColor("PulseColor", Color) = (1, 0, 0, 1)
        _PulseTex ("PulseTexture", 2D) = "white" {}
		_PulseOpacity("PulseOpacity", Float) = 1
		_MinPulseOpacity("MinimumPulseOpacity", Range(0, 1))=0
		_MinAnimOpacity("MinimumAnimationOpacity", Range(0, 1))=0
		_PulseIntensity("PulseIntensity", Float)=1
		_PulseSpeed("PulseSpeed", Float)=1
		_PulseHorizontalDist("PulseHorizontalDist", Float)=5
		_PulseTexOffsetScale("PulseTexOffset", Float)=1

		_EdgeColor("EdgeColor", Color) = (1, 0, 0, 1)
		_EdgeOpacity("EdgeOpacity", Float)=1
		_EdgeTex("PulseEdgeTexture", 2D) = "white"{}
		_EdgeIntensity("EdgeIntensity", Float)=1
		_MinEdgeOpacity("MinimumEdgeOpacity", Range(0, 1)) = 0
		_EdgeSpeed("EdgeSpeed", Float)=1
		_EdgePauses("EdgePauseIncrease", Range(0, 1))=1
		_EdgeAnimWidth("EdgeAnimationWidth", Float)=3

		_BorderTex("BorderTexture", 2D)="white"{}
		_BorderOpacity("BorderOpacity", Float)=1
		_BorderColor("BorderColor", Color)=(0, 0, 1, 1)
		_BorderIntensity("BorderIntensity", Float)=1

    }
    SubShader
    {
        Tags { "RenderType"="Transparent" "Queue"="Transparent" }
		Cull Off
		Blend SrcAlpha OneMinusSrcAlpha
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            // make fog work
            //#pragma multi_compile_fog

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float2 uv1 : TEXCOORD1;
                float2 uv2 : TEXCOORD2;
                //UNITY_FOG_COORDS(1)
                float4 vertex : SV_POSITION;
				float4 vertexLocal : TEXCOORD10;
            };

			//Pulse properties
            sampler2D _PulseTex;
            float4 _PulseTex_ST;
			
			float4 _PulseColor;
			float _PulseOpacity;
			float _MinPulseOpacity;
			float _MinAnimOpacity;
			float _PulseIntensity;
			float _PulseSpeed;
			float _PulseHorizontalDist;
			float _PulseTexOffsetScale;

			//Edge properties
			sampler2D _EdgeTex;
			float4 _EdgeTex_ST;

			float4 _EdgeColor;
			float _EdgeOpacity;
			float _EdgeIntensity;
			float _MinEdgeOpacity;
			float _EdgeSpeed;
			float _EdgePauses;
			float _EdgeAnimWidth;

			//Border
			sampler2D _BorderTex;
			float4 _BorderTex_ST;

			float4 _BorderColor;
			float _BorderOpacity;
			float _BorderIntensity;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _PulseTex);
                o.uv1 = TRANSFORM_TEX(v.uv, _EdgeTex);
                o.uv2 = TRANSFORM_TEX(v.uv, _BorderTex);
				o.vertexLocal = v.vertex;
                UNITY_TRANSFER_FOG(o,o.vertex);
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
				//UNITY_APPLY_FOG(i.fogCoord, col);

				float3 worldScale = float3(
						length(float3(unity_ObjectToWorld[0].x, unity_ObjectToWorld[1].x, unity_ObjectToWorld[2].x)), // scale x axis
						length(float3(unity_ObjectToWorld[0].y, unity_ObjectToWorld[1].y, unity_ObjectToWorld[2].y)), // scale y axis
						length(float3(unity_ObjectToWorld[0].z, unity_ObjectToWorld[1].z, unity_ObjectToWorld[2].z))  // scale z axis
						);

				//pulse
                fixed4 pulseTex = tex2D(_PulseTex, i.uv*float2(worldScale.x, 1));
				float timeDistortion = _MinAnimOpacity+((1-_MinAnimOpacity)*_PulseIntensity*(0.5f*(1.0f + sin(_Time.y*_PulseSpeed +pulseTex.r*_PulseTexOffsetScale-abs(i.vertexLocal.x)*_PulseHorizontalDist*worldScale.x))));

                
				fixed3 pulseColor = _PulseColor*_MinPulseOpacity+_PulseColor *	(pulseTex.r)* timeDistortion;

				//edge
				fixed4 edgeTex = tex2D(_EdgeTex, i.uv1*float2(worldScale.x, 1));
				float edgeTime = (1 / (1 - _EdgePauses))* 
					max(sin(_Time.y*_EdgeSpeed-(abs(i.vertexLocal.x*1/(_EdgeTex_ST.x*1/worldScale.x))+abs(i.vertexLocal.y))*_EdgeAnimWidth) -_EdgePauses, 0);
				fixed3 edgeColor =  _EdgeColor * edgeTex.r * _EdgeIntensity*max(_MinEdgeOpacity, edgeTime);

				//border
				fixed4 borderTex = tex2D(_BorderTex, i.uv2);

				fixed3 col =
					pulseColor * (1 - edgeTex.r) * _PulseOpacity
					+ edgeColor * _EdgeOpacity;
				col = lerp(col, _BorderColor*_BorderIntensity*_BorderOpacity, borderTex.r);

				float opacity = borderTex.g;

                return fixed4(col, opacity);
            }
            ENDCG
        }
    }
}
