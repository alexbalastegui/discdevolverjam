﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileCollisionChecker : MonoBehaviour
{
    public float colliderScalar = .75f;
    void Update()
    {
        Collider[] colliders = Physics.OverlapBox(transform.position,transform.parent.localScale*colliderScalar, transform.rotation);

        ExtDebug.DrawBox(transform.position, transform.parent.localScale * colliderScalar, transform.rotation, Color.red);

        foreach (Collider c in colliders)
        {
            if (c.GetComponent<BulletMovement>() != null)
            {
                c.GetComponent<BulletMovement>().bulletHit();
                GetComponent<PlayerController>().Damage(1);
                Debug.Log("Player hit");
                if (GameMaster.Instance.Pooler != null)
                    c.gameObject.SetActive(false);
                else
                    Destroy(c.gameObject);
            }
        }
    }
    //void OnDrawGizmos()
    //{
    //    Gizmos.color = Color.red;
    //    Gizmos.DrawWireCube(transform.position, transform.parent.localScale * colliderScalar);
    //}
}
