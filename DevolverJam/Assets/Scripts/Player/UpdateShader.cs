﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpdateShader : MonoBehaviour
{

    void Update()
    {
        Shader.SetGlobalVector("Player_position", transform.position);
    }
}
