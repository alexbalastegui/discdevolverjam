﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputNameScore : MonoBehaviour{

    string scoreName;

    [SerializeField] GameObject nextScreen = default;
    [SerializeField] Percentage percentage = default;

    void Awake(){
        nextScreen.SetActive(false);
    }

    public void InputName(string name){
        scoreName = name;
    }

    public void Submit(){
        if(scoreName.ToCharArray().Length == 3){
            PlayerController player = FindObjectOfType<PlayerController>();
            AddHighscoreEntry(Mathf.RoundToInt(player.timeSurvived), player.percentage, scoreName.ToUpper(), player.bossKilled);
            gameObject.SetActive(false);
            nextScreen.SetActive(true);
            percentage.UpdateSlider();
        }
    }

    void AddHighscoreEntry(int time, int percentage, string name, bool boss){
        HighscoreEntry highscoreEntry = new HighscoreEntry { name = name, percentage = percentage, time = time, boss = boss };

        string jsonString = PlayerPrefs.GetString("HighscoreTable");
        Highscores highscores = JsonUtility.FromJson<Highscores>(jsonString);

        highscores.highscoreEntryList.Add(highscoreEntry);

        string json = JsonUtility.ToJson(highscores);
        PlayerPrefs.SetString("HighscoreTable", json);
        PlayerPrefs.Save();
    }

    class Highscores{
        public List<HighscoreEntry> highscoreEntryList;
    }

    [System.Serializable]
    class HighscoreEntry{
        public bool boss;
        public int time;
        public int percentage;
        public string name;
    }
}
