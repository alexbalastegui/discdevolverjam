﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DistanceConstraint : MonoBehaviour{

    public Transform source;
    public float maxDistance;
    [Range(0, 1)] public float maxHeight = .15f;

    void Update(){
        if ((transform.position - source.position).magnitude > maxDistance){
            transform.position = source.position + (transform.position - source.position).normalized * maxDistance;
            transform.localPosition = new Vector3(transform.localPosition.x, maxHeight, transform.localPosition.z);
        }
    }
}
