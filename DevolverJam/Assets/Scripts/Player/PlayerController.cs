﻿using Cinemachine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour{

    [Space]
    [HideInInspector] public bool bossKilled;
    [HideInInspector] public float timeSurvived;
    public int percentage;

    [SerializeField] Transform container = default;
    Rigidbody rb;
    [SerializeField] float speed = 6f;
    [SerializeField] LayerMask whatIsGround = -1;
    [SerializeField] Animator anim = default;
    float rotationVelocity;
    [SerializeField] ParticleSystem stepParticle = default;
    bool walking;

    [Space]
    public int weaponSelected;
    [SerializeField] Transform shootPos = default;
    [SerializeField] Animator deathScreenAnim = default;
    [SerializeField] Animator winScreenAnim = default;

    [Space]
    [SerializeField] Transform aimPos = default;
    public PlayerHealth health;
    Camera mainCamera;

    [SerializeField, Range(0, .5f)] float timeBetweenSteps = .1f;
    float currentStepTime;
    bool step;

    [SerializeField] ParticleSystem shootParticles = default;
    [SerializeField] Animator getDiskIcon;

    [HideInInspector]
    public UnityEngine.Events.UnityEvent damageEv=new UnityEngine.Events.UnityEvent();

    [SerializeField] CinemachineVirtualCamera trackedCam = default;
    [SerializeField, Range(0, 1)] float camRotationSpeed = .2f;
    [SerializeField] Transform track = default;
    CinemachineTrackedDolly cmTrack;
    bool dead;

    [HideInInspector] public bool invencible;

    bool wayToTheBoss;

    // Start is called before the first frame update
    void Awake(){
        timeSurvived = PlayerPrefs.GetFloat("TimeSurvived", 0);
        percentage = PlayerPrefs.GetInt("Percentage", 1);
        rb = GetComponent<Rigidbody>();
        weaponSelected = 1;
        health = FindObjectOfType<PlayerHealth>();
        mainCamera = Camera.main;
        trackedCam.gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update(){
        percentage = Mathf.Clamp(percentage, 0, 100);
        if (!wayToTheBoss && percentage >= 100){
            wayToTheBoss = true;
            FindObjectOfType<Portal>().bossPortal = true;
        }

        Vector3 movementInput = new Vector3(Input.GetAxisRaw("Horizontal"), 0, Input.GetAxisRaw("Vertical")).normalized;

        Ray ray = mainCamera.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

        if(Physics.Raycast(ray, out hit, 1000f, whatIsGround) && Time.timeScale != 0 && Input.GetMouseButtonDown(0) && health && health.currentHp > 0){
            Vector3 pos = hit.point - transform.position;
            float rotY = Mathf.Atan2(-pos.z, pos.x) * Mathf.Rad2Deg;
            transform.rotation = Quaternion.Euler(0f, rotY + 90, 0f);
        }

        if (Input.GetMouseButtonDown(0) && Time.timeScale != 0 && health && health.currentHp > 0){
            FrisbieController frisbie = ObjectPooler.instance.SpawnFromPool("Disk", new Vector3(shootPos.position.x, .3f, shootPos.position.z), transform.rotation).GetComponent<FrisbieController>();
            //FrisbieController frisbie = Instantiate(disk, transform.position, transform.rotation).GetComponent<FrisbieController>();
            if(AudioManager.instance!=null)
                AudioManager.instance.PlayOneShot("laser" + Random.Range(1, 6));
            shootParticles.Play();
            switch (weaponSelected){
                case 1:
                    frisbie.frisbieType = FrisbieController.FrisbieType.Default;
                    frisbie.AddForce(movementInput * speed);
                    break;
                case 2:
                    frisbie.frisbieType = FrisbieController.FrisbieType.Boomerang;
                    frisbie.AddForce(movementInput * speed);
                    break;
                case 3:
                    frisbie.frisbieType = FrisbieController.FrisbieType.Explosive;
                    frisbie.AddForce(movementInput * speed);
                    break;
            }
            health.RemoveDisk();
            if (health.currentHp == 0){
                AudioManager.instance.Pause("InGameMusic");
                AudioManager.instance.Play("clock2-loop");
                deathScreenAnim.SetTrigger("Die");
            }
            if (ScreenShakeCall.instance!=null)
                ScreenShakeCall.instance.ShakeCamera(.5f, .25f);
        }

        if (dead){
            cmTrack.m_PathPosition += Time.unscaledDeltaTime * camRotationSpeed;
        }else{
            timeSurvived += Time.deltaTime;
        }
    }

    void FixedUpdate(){
        Vector3 movementInput = new Vector3(Input.GetAxisRaw("Horizontal"), 0, Input.GetAxisRaw("Vertical")).normalized;
        anim.SetFloat("Speed", movementInput.magnitude);
        if (movementInput.magnitude >= .1f && !Input.GetMouseButtonDown(0)){
            float targetAngle = Mathf.Atan2(movementInput.x, movementInput.z) * Mathf.Rad2Deg + mainCamera.transform.eulerAngles.y;
            float angle = Mathf.SmoothDampAngle(transform.eulerAngles.y, targetAngle, ref rotationVelocity, .1f);
            transform.rotation = Quaternion.Euler(0, angle, 0);

            Vector3 moveDirection = Quaternion.Euler(0, targetAngle, 0) * Vector3.forward;
            //rb.MovePosition(rb.position + moveDirection.normalized * speed * Time.deltaTime);
            rb.velocity = moveDirection.normalized * speed;

            currentStepTime -= Time.deltaTime;
            if(currentStepTime <= 0 && AudioManager.instance!=null){
                stepParticle.Play();
                if (step){
                    AudioManager.instance.PlayOneShot("paso1");
                    step = false;
                }else{
                    AudioManager.instance.PlayOneShot("paso2");
                    step = true;
                }
                currentStepTime = timeBetweenSteps;
            }
        }else{
            rb.velocity = Vector3.zero;
        }

        if(movementInput.magnitude >= .1f && !Input.GetMouseButtonDown(0) && !walking){
            currentStepTime = 0;
            walking = true;
        }else if(movementInput.magnitude < .1f && !Input.GetMouseButtonDown(0)){
            walking = false;
        }
    }

    void LateUpdate(){
        Ray ray = mainCamera.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit, 1000f, whatIsGround) && Time.timeScale != 0){
            aimPos.position = new Vector3(hit.point.x, aimPos.position.y, hit.point.z);
        }

        Vector3 targetVector = transform.position - mainCamera.transform.position;
        getDiskIcon.transform.rotation = Quaternion.LookRotation(targetVector, mainCamera.transform.rotation * Vector3.up);
    }

    void damageSheen()
    {
        Debug.Log("sheeen");
        foreach (SkinnedMeshRenderer mr in GetComponentsInChildren<SkinnedMeshRenderer>())
            mr.material.SetColor("EmissiveCol", Color.white);
        Invoke(nameof(revertDamageSheen), .1f);
    }
    void revertDamageSheen()
    {
        foreach (SkinnedMeshRenderer mr in GetComponentsInChildren<SkinnedMeshRenderer>())
            mr.material.SetColor("EmissiveCol", Color.black);
    }

    public void Damage(int amt){
        if (invencible)
            return;

        float randomSound = Random.Range(0, 300);
        if(randomSound < 100){
            AudioManager.instance.PlayOneShot("grito2");
        }else if(randomSound >= 100 && randomSound < 200){
            AudioManager.instance.PlayOneShot("grito5");
        }else{
            AudioManager.instance.PlayOneShot("grito11");
        }

        if (health.currentHp == 0){
            Die();
            return;
        }

        for (int i = 0; i < amt; i++){
            damageSheen();
            if (health != null){
                health.RemoveDisk();
                damageEv.Invoke();
            }
        }
        
        if (health.currentHp == 0){
            AudioManager.instance.Pause("InGameMusic");
            AudioManager.instance.Play("clock2-loop");
            deathScreenAnim.SetTrigger("Die");
        }

        if (ScreenShakeCall.instance!=null)
            ScreenShakeCall.instance.ShakeCamera(1, .5f);
    }

    void OnTriggerEnter(Collider col){
        if (col.CompareTag("Disk") || col.CompareTag("DiskPickup")){
            if (health.currentHp == 0){
                AudioManager.instance.UnPause("InGameMusic");
                StartCoroutine(AudioManager.instance.FadeOut("clock2-loop", .5f));
                deathScreenAnim.SetTrigger("GetDisk");

                getDiskIcon.SetTrigger("GetDisk");
                health.AddDisk();
                col.transform.parent.gameObject.SetActive(false);
            }else{
                if (health.AddDisk()){
                    getDiskIcon.SetTrigger("GetDisk");
                    AudioManager.instance.PlayOneShot("recogerdisco2");
                    col.transform.parent.gameObject.SetActive(false);
                }
            }
        }
    }

    public void Die(){
        GameMaster.Instance.ResetLevel();
        track.position = new Vector3(transform.position.x, 0, transform.position.z);
        trackedCam.gameObject.SetActive(true);
        cmTrack = trackedCam.GetCinemachineComponent<CinemachineTrackedDolly>();
        dead = true;
        Time.timeScale = 0;
        deathScreenAnim.updateMode = AnimatorUpdateMode.UnscaledTime;
        PlayerPrefs.DeleteKey("HP");
    }

    public void Win(){
        GameMaster.Instance.ResetLevel();
        winScreenAnim.SetTrigger("Win");
        dead = true;
        PlayerPrefs.DeleteKey("HP");
        StartCoroutine(WinPause());
    }

    IEnumerator WinPause(){
        yield return new WaitForSeconds(1f);
        StartCoroutine(AudioManager.instance.FadeIn("MainPiano", 1));
        FindObjectOfType<PauseMenu>().enabled = false;
        winScreenAnim.updateMode = AnimatorUpdateMode.UnscaledTime;
        Time.timeScale = 0;
    }
}
