﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerDeath : MonoBehaviour{

    [SerializeField] PlayerController player = default;

    public void Die(){
        player.Die();
    }
}
