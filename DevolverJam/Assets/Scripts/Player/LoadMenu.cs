﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadMenu : MonoBehaviour{
    public void LoadLevel(){
        Destroy(GameMaster.Instance.gameObject);
        PlayerPrefs.DeleteKey("HP");
        PlayerPrefs.DeleteKey("Percentage");
        PlayerPrefs.DeleteKey("TimeSurvived");
        Loader.Load(0);
    }
}
