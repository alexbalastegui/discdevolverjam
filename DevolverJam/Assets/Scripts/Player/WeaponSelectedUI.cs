﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponSelectedUI : MonoBehaviour{

    int currentSelected = 0;
    [SerializeField] Transform rollPivot = default;
    float currentRot;
    float rotationVelocity;
    PlayerController player;

    // Start is called before the first frame update
    void Awake(){
        currentRot = rollPivot.eulerAngles.z;
        player = GameMaster.Instance.player.GetComponent<PlayerController>();
    }

    // Update is called once per frame
    void Update(){
        currentRot = 90 * currentSelected;
        //currentRot += 90 * Mathf.Clamp(Input.mouseScrollDelta.y, -1, 1);
        if(Mathf.Clamp(Input.mouseScrollDelta.y, -1, 1) == 1){
            switch (currentSelected){
                case 0:
                    Debug.Log("Boomerang");
                    player.weaponSelected = 2;
                    currentSelected = 1;
                    break;
                case 1:
                    Debug.Log("Default");
                    player.weaponSelected = 1;
                    currentSelected = 2;
                    break;
                case 2:
                    Debug.Log("Explosive");
                    currentSelected = 3;
                    player.weaponSelected = 3;
                    break;
                case 3:
                    Debug.Log("Default");
                    currentSelected = 0;
                    player.weaponSelected = 1;
                    break;
            }
        }else if(Mathf.Clamp(Input.mouseScrollDelta.y, -1, 1) == -1){
            switch (currentSelected){
                case 0:
                    Debug.Log("Explosive");
                    currentSelected = 3;
                    player.weaponSelected = 3;
                    break;
                case 1:
                    Debug.Log("Default");
                    player.weaponSelected = 1;
                    currentSelected = 0;
                    break;
                case 2:
                    Debug.Log("Boomerang");
                    player.weaponSelected = 2;
                    currentSelected = 1;
                    break;
                case 3:
                    Debug.Log("Default");
                    player.weaponSelected = 1;
                    currentSelected = 2;
                    break;
            }
        }
        rollPivot.rotation = Quaternion.Euler(0, 0, Mathf.SmoothDampAngle(rollPivot.eulerAngles.z, currentRot, ref rotationVelocity, .1f));
    }
}
