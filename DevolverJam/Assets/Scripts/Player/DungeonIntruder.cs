﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class DungeonIntruder : MonoBehaviour
{
    public Room currentRoom;
    public UnityEvent roomChange;

    private void Awake()
    {
        roomChange = new UnityEvent();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.GetComponent<Room>() != null && other.gameObject.GetComponent<Room>()!=currentRoom)
        {
            currentRoom = other.gameObject.GetComponent<Room>();
            roomChange.Invoke();
        }
    }
}
