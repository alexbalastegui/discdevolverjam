﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHealth : MonoBehaviour{

    [SerializeField] GameObject disk = default;
    [SerializeField, Min(0)] int startHp = 10;
    [SerializeField] RectTransform healthStartPos = default;
    public int currentHp;
    [SerializeField, Range(0, 100)] float distanceBetweenDisks = 30f;
    Vector3 nextDiskPos;

    List<Animator> diskAnimators = new List<Animator>();

    [Space]
    [SerializeField, Min(0)] int maxHp = 15;
    [SerializeField] bool hasMaxHp = false;

    // Start is called before the first frame update
    void Start(){
        currentHp = PlayerPrefs.GetInt("HP", startHp);
        if (currentHp == 0)
            currentHp = startHp;

        for (int i = 0; i < currentHp; i++){
            GameObject newDisk = Instantiate(disk, Vector3.zero, Quaternion.identity, transform);
            newDisk.GetComponent<RectTransform>().anchoredPosition3D = healthStartPos.anchoredPosition3D + Vector3.right * distanceBetweenDisks * i;
            diskAnimators.Add(newDisk.GetComponent<Animator>());
            nextDiskPos = Vector3.right * distanceBetweenDisks * (i + 1);
        }
    }

    public bool AddDisk(){
        if (hasMaxHp && currentHp >= maxHp)
            return false;

        foreach(Animator a in diskAnimators){
            a.SetTrigger("RestartAnimation");
        }
        GameObject newDisk = Instantiate(disk, Vector3.zero, Quaternion.identity, transform);
        newDisk.GetComponent<RectTransform>().anchoredPosition3D = healthStartPos.anchoredPosition3D + nextDiskPos;
        diskAnimators.Add(newDisk.GetComponent<Animator>());
        nextDiskPos += Vector3.right * distanceBetweenDisks;
        currentHp++;
        PlayerPrefs.SetInt("HP", currentHp);
        return true;
    }

    public void RemoveDisk(){
        if(currentHp > 0){
            Animator anim = diskAnimators[diskAnimators.Count - 1];
            diskAnimators.Remove(anim);
            Destroy(anim.gameObject);
            nextDiskPos -= Vector3.right * distanceBetweenDisks;
            currentHp--;
            PlayerPrefs.SetInt("HP", currentHp);
        }
    }
}
