﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class sinMovement : MonoBehaviour
{
    [Header("Idle")]
    public float idleAngle = 5;
    public float idleSpeed = 1;

    [Header("Damaged")]
    public float damagedAngle=-5;
    private float spasm = 0;
    [Space(5)]
    private bool death = false;
    public float deathAngle = 20;
    public float deathSpeed = 2;
    public float deathDuration = 5;
    public BossJointsIK ik;
    private float deathSpasm = 0;
    bool update = true;

    private float startZ = 0;

    private void Start()
    {
        startZ = transform.eulerAngles.z;
    }

    private float targetZ = 0;

    private void FixedUpdate()
    {
        if (update)
        {
            if (!death)
                targetZ = startZ + Mathf.Sin(Time.time * idleSpeed) * idleAngle - (damagedAngle * spasm);
            else
                targetZ = startZ + Mathf.Sin(Time.time * deathSpeed) * deathAngle * deathSpasm;
        }
    }

    void Update()
    {
        if(update)
            transform.eulerAngles = Vector3.Lerp(transform.eulerAngles, new Vector3(transform.eulerAngles.x, transform.eulerAngles.y, targetZ), 0.1f);
    }

    public void HarmSpasm()
    {
        LeanTween.value(0, 1, 0.01f).setOnUpdate(updateMuscle).setEaseInBack().setOnComplete(relaxMuscle);
    }
    void updateMuscle(float v)
    {
        spasm = v;
    }
    void relaxMuscle()
    {
        LeanTween.value(1, 0, 0.7f).setOnUpdate(updateMuscle).setEaseOutBack();
    }

    public void deathJiggle()
    {
        death = true;
        Debug.Log("wiggle");
        LeanTween.value(0, 1, 0.2f).setOnUpdate(updateDeath).setEaseLinear().setLoopPingPong();
        Invoke(nameof(KO), deathDuration);
    }
    private void updateDeath(float f)
    {
        deathSpasm = f;
    }

    private void KO()
    {
        update = false;
        ik.enabled = false;
        ik.joint1.localEulerAngles = new Vector3(0, 0, 40);
        ik.joint2.localEulerAngles = new Vector3(-180, 183.4f, 294.512f);
        ik.endEffector.localEulerAngles = new Vector3(357, 4.79f, 34.803f);

    }
}
