﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class collisionMessageTeller : MonoBehaviour
{
    public BossBehaviour boss;

    private void OnCollisionEnter(Collision collision)
    {
        boss.OnCollisionEnterChild(collision);
    }
}
