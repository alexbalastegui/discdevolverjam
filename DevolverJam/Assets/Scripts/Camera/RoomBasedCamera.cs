﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoomBasedCamera : MonoBehaviour
{
    public Vector3 cameraAngle=Vector3.zero;
    private float armLength=20;
    public Vector3 tweakVector = Vector3.zero;
    public LeanTweenType easeType = LeanTweenType.easeInOutCubic;
    public float easeTime = 0.5f;

    private GameObject player;

    private void Awake()
    {
        GameMaster.Instance.player.GetComponent<DungeonIntruder>().roomChange.AddListener(swapFocus);
        player=GameMaster.Instance.player;
    }

    private void Start()
    {
        transform.eulerAngles = cameraAngle;
        if(GameMaster.Instance.Dungeon!=null && GameMaster.Instance.Dungeon.EntryRoom!=null)
            transform.position = GameMaster.Instance.Dungeon.EntryRoom.transform.position;
        transform.position += transform.forward * -armLength + tweakVector;
    }

    void swapFocus()
    {
        transform.LeanMove(player.GetComponent<DungeonIntruder>().currentRoom.gameObject.transform.position+ transform.forward * -armLength+tweakVector, easeTime).setEase(easeType);
    }

    
}
