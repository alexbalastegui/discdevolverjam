﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CineMachineUpdater : MonoBehaviour
{
    Cinemachine.CinemachineVirtualCamera _camera;


    private void Awake()
    {
        _camera = GetComponent<Cinemachine.CinemachineVirtualCamera>();
        _camera.Follow = GameMaster.Instance.player.transform.parent.GetComponentInChildren<WeightedLocation>().transform;
    }
    void setUP()
    {
        _camera.Follow = GameMaster.Instance.player.transform.parent.gameObject.GetComponent<WeightedLocation>().gameObject.transform;
    }

}
