﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuLoader : MonoBehaviour{

    public void LoadMenu(){
        PlayerPrefs.DeleteKey("HP");
        PlayerPrefs.DeleteKey("Percentage");
        PlayerPrefs.DeleteKey("TimeSurvived");
        Time.timeScale = 1;
        Destroy(GameMaster.Instance.gameObject);
        Loader.Load(0);
    }
}
