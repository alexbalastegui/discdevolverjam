﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public static class Loader{

    class LoadingMonoBehaviour : MonoBehaviour { }

    static Action onLoaderCallback;
    static AsyncOperation loadingAsyncOperation;

    public static void Load(int scene){
        onLoaderCallback = () => {
            GameObject loadingGameObject = new GameObject("LoadingGameObject");
            loadingGameObject.AddComponent<LoadingMonoBehaviour>().StartCoroutine(LoadSceneAsync(scene));
        };

        SceneManager.LoadScene("Loading");
    }

    static IEnumerator LoadSceneAsync(int scene){
        yield return null;
        loadingAsyncOperation = SceneManager.LoadSceneAsync(scene);

        while (!loadingAsyncOperation.isDone){
            yield return null;
        }
    }

    public static float GetLoadingProgress(){
        if(loadingAsyncOperation != null){
            return loadingAsyncOperation.progress;
        }else{
            return 0;
        }
    }

    public static void LoaderCallback(){
        if(onLoaderCallback != null){
            onLoaderCallback();
            onLoaderCallback = null;
        }
    }
}
