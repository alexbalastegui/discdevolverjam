﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class FakeShopSeller : MonoBehaviour{

    [SerializeField] GameObject sellerText = default;
    [SerializeField] TMP_Animated text = default;
    [SerializeField] Dialogue dialogue = default;
    [SerializeField] GameObject arrow = default;

    void Awake(){
        arrow.SetActive(false);
    }

    void OnTriggerEnter(Collider col){
        if (col.CompareTag("Player")){
            arrow.SetActive(true);
        }
    }

    void OnTriggerStay(Collider col){
        if (col.CompareTag("Player")){
            if(Input.GetKeyDown(KeyCode.Space) || Input.GetMouseButtonDown(0)){
                Time.timeScale = 0;
                sellerText.SetActive(true);
                text.ClearText();
                text.ReadText(dialogue.sentences[0]);
            }
        }
    }

    void OnTriggerExit(Collider col){
        if (col.CompareTag("Player")){
            arrow.SetActive(false);
        }
    }

    public void EndConversation(){
        Time.timeScale = 1;
    }
}
