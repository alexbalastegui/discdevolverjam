﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeightedLocation : MonoBehaviour
{
    [System.Serializable]
    public struct WeightedTarget
    {
        public Transform t;
        public float weight;
    }


    public List<WeightedTarget> targets;

    private void Update()
    {
        transform.position = Vector3.zero;
        Vector3 pos = Vector3.zero;
        Vector3 pivot = targets[0].t.position;

        float factor = 0;
        foreach(WeightedTarget t in targets)
        {
            pos += t.t.position * t.weight;;
            factor += t.weight;
        }
        pos /= (factor);
        transform.position = pos;
    }
}
