﻿[System.Serializable]
public class PlayerData{

    public bool bossKilled;
    public float playDuration;

    public PlayerData(PlayerController player){
        bossKilled = player.bossKilled;
        playDuration = player.timeSurvived;
    }
}
