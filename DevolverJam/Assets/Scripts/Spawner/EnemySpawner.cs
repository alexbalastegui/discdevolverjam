﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour{

    [System.Serializable] enum EnemyType{Cookie, HDD, Turret, Folder, LED, LevelTurret}
    [SerializeField] EnemyType enemy = EnemyType.Cookie;
    [SerializeField, Range(0, 1)] float spawnHeight = 0;

    bool canSpawn;

    void Awake(){
        transform.Find("Cube").gameObject.SetActive(false);
    }

    void Start(){
        canSpawn = ObjectPooler.instance != null;
    }

    public void Spawn(RoomTrigger room){
        if (canSpawn){
            switch (enemy){
                case EnemyType.Cookie:
                    CookieController cookie = ObjectPooler.instance.SpawnFromPool("Cookie", new Vector3(transform.position.x, spawnHeight, transform.position.z)).GetComponent<CookieController>();
                    room.enemies.Add(cookie.gameObject);
                    cookie.room = room;
                    break;
                case EnemyType.HDD:
                    HardDriveController hdd = ObjectPooler.instance.SpawnFromPool("HDD", new Vector3(transform.position.x, spawnHeight, transform.position.z)).GetComponent<HardDriveController>();
                    room.enemies.Add(hdd.gameObject);
                    hdd.room = room;
                    break;
                case EnemyType.Turret:
                    TurretController turret = ObjectPooler.instance.SpawnFromPool("Turret", new Vector3(transform.position.x, spawnHeight, transform.position.z)).GetComponent<TurretController>();
                    turret.room = room;
                    room.enemies.Add(turret.gameObject);
                    break;
                case EnemyType.Folder:
                    FolderController folder = ObjectPooler.instance.SpawnFromPool("Folder", new Vector3(transform.position.x, spawnHeight, transform.position.z)).GetComponent<FolderController>();
                    folder.room = room;
                    room.enemies.Add(folder.gameObject);
                    break;
                case EnemyType.LED:
                    GusilightController led = ObjectPooler.instance.SpawnFromPool("LED", new Vector3(transform.position.x, spawnHeight, transform.position.z)).GetComponent<GusilightController>();
                    led.room = room;
                    room.enemies.Add(led.gameObject);
                    break;
                case EnemyType.LevelTurret:
                    break;
            }
        }
    }
}
