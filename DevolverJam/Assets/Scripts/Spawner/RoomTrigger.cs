﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[System.Serializable] public class OnEnter : UnityEvent { }

public class RoomTrigger : MonoBehaviour{

    [SerializeField] GameObject laser1Object = default;
    [SerializeField] bool laser1 = false;
    [SerializeField] GameObject laser2Object = default;
    [SerializeField] bool laser2 = false;
    [SerializeField] GameObject laser3Object = default;
    [SerializeField] bool laser3 = false;
    [SerializeField] GameObject laser4Object = default;
    [SerializeField] bool laser4 = false;

    [Space]
    [SerializeField] OnEnter onEnter = default;
    bool triggered;
    [HideInInspector] public List<GameObject> enemies;

    GameObject navMesh;

    void Awake(){
        laser1Object.SetActive(false);
        laser2Object.SetActive(false);
        laser3Object.SetActive(false);
        laser4Object.SetActive(false);
        enemies = new List<GameObject>();
    }

    void Start(){
        navMesh = transform.parent.parent.Find("NavigationPrefab").Find("NavMesh").gameObject;
        navMesh.SetActive(false);
    }

    void OnTriggerEnter(Collider col){
        if (col.CompareTag("Player") && !triggered){
            navMesh.SetActive(true);
            if (laser1){
                laser1Object.SetActive(true);
            }
            if (laser2){
                laser2Object.SetActive(true);
            }
            if (laser3){
                laser3Object.SetActive(true);
            }
            if (laser4){
                laser4Object.SetActive(true);
            }
            triggered = true;
            onEnter.Invoke();
        }
    }

    public void Die(GameObject enemy){
        enemies.Remove(enemy);
        if (enemies.Count == 0){
            navMesh.SetActive(false);
            laser1Object.SetActive(false);
            laser2Object.SetActive(false);
            laser3Object.SetActive(false);
            laser4Object.SetActive(false);
        }
    }
}
