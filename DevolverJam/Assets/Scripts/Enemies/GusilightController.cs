﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class GusilightController : MonoBehaviour, IPooledObject{

    [Header("Lifes")]
    [SerializeField] GameObject life1 = default;
    [SerializeField] GameObject life2 = default;
    [SerializeField] GameObject life3 = default;
    int life;
    Rigidbody rb;

    [Space]
    [SerializeField, Range(0, 1)] float rotationSpeed = .5f;
    [SerializeField, Range(0, 3)] float attackRotationSpeed = 1.5f;
    [SerializeField] float regainMobilityDelay = 1f;
    [SerializeField] float patrolingSpeed = 2f;
    [SerializeField] float speed = 4f;
    [SerializeField] Animator anim = default;

    bool patroling, rotationSelected, playerDetected, walking;
    Vector3 posToGo;
    PlayerController player;
    BoxCollider boxCollider;

    [HideInInspector] public EnemySpawner spawner;
    [HideInInspector] public RoomTrigger room;

    NavMeshAgent agent;

    // Start is called before the first frame update
    void Awake(){
        boxCollider = GetComponent<BoxCollider>();
        player = FindObjectOfType<PlayerController>();
        agent = GetComponent<NavMeshAgent>();
        agent.speed = speed;
        rb = GetComponent<Rigidbody>();
    }

    public void OnObjectSpawn(){
        life1.SetActive(false);
        life2.SetActive(false);
        life3.SetActive(false);

        rb.isKinematic = true;
        boxCollider.isTrigger = false;
        agent.Warp(transform.position);
        playerDetected = false;
        life = 3;
    }

    void Update(){
        if (playerDetected){
            if (walking){
                agent.SetDestination(player.transform.position);
                agent.isStopped = false;
            }
        }else{
            if (!patroling && !rotationSelected){
                posToGo = transform.position + new Vector3(Random.Range(-5f, 5f), 0, Random.Range(-5f, 5f));
                StartCoroutine(Patrol());
                rotationSelected = true;
                patroling = true;
            }
        }
    }

    void OnCollisionEnter(Collision col){
        if (col.gameObject.CompareTag("Disk")){
            if (!playerDetected){
                rb.isKinematic = false;
                playerDetected = true;
                walking = true;
            }else{
                switch (life){
                    case 3:
                        life3.SetActive(true);
                        break;
                    case 2:
                        life2.SetActive(true);
                        break;
                    case 1:
                        life1.SetActive(true);
                        agent.isStopped = true;
                        break;
                }
                life--;
                if (life == 0){
                    gameObject.SetActive(false);
                    player.percentage += Utils.RAM_cleanseBig;
                    room.Die(gameObject);
                    GameMaster.Instance.Pooler.SpawnFromPool("EnemyExplosion", transform.position);
                    if (Utils.spawnDisk())
                        GameMaster.Instance.Pooler.SpawnFromPool("DiskPickup", transform.position);
                }
            }

            col.gameObject.GetComponent<Rigidbody>().velocity = Vector3.zero;
        }
    }

    void OnTriggerEnter(Collider col){
        if (col.CompareTag("Disk")){
            if (!playerDetected){
                rb.isKinematic = false;
                playerDetected = true;
                walking = true;
            }else{
                switch (life){
                    case 3:
                        life3.SetActive(true);
                        break;
                    case 2:
                        life2.SetActive(true);
                        break;
                    case 1:
                        life1.SetActive(true);
                        agent.isStopped = true;
                        break;
                }
                life--;
                if(life == 0){
                    player.percentage += Utils.RAM_cleanseBig;
                    gameObject.SetActive(false);
                    room.Die(gameObject);
                    GameMaster.Instance.Pooler.SpawnFromPool("EnemyExplosion", transform.position);
                    if (Utils.spawnDisk())
                        GameMaster.Instance.Pooler.SpawnFromPool("DiskPickup", transform.position);
                }
            }

            col.GetComponent<Rigidbody>().velocity = Vector3.zero;
        }
    }

    IEnumerator Patrol(){
        agent.isStopped = false;
        agent.SetDestination(posToGo);
        yield return new WaitForSeconds(Random.Range(1f, 2f));
        rb.velocity = Vector3.zero;
        patroling = false;
        rotationSelected = false;
        agent.isStopped = !playerDetected;
    }
}
