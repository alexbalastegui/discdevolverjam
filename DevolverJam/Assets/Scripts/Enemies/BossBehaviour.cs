﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

[System.Serializable]
public class BossState
{
    [Range(0, 101)]
    public float healthActivation;
    public float activityTime;
    public float recoverTime;
    public ShootingPattern.ShootingStyle[] turretStyle=new ShootingPattern.ShootingStyle[7];
    public BulletMovement.BulletMovementType[] bulletStyle=new BulletMovement.BulletMovementType[7];

    [HideInInspector]
    public bool used = false;


    public BossState()
    {
        turretStyle = new ShootingPattern.ShootingStyle[7];
        bulletStyle = new BulletMovement.BulletMovementType[7];
    }
}

public class BossBehaviour : MonoBehaviour
{
    [Header("Imperative references")]
    public SkinnedMeshRenderer myMesh;
    public TurretCoil_Behaviour[] turrets;
    public Image sheenImage;
    public UnityEvent harmEv;
    public UnityEvent dieEv;

    [Header("General settings")]
    public float HP = 100;
    public int damageTaken = 2;
    public bool startPassive = false;
    public BossState[] states;

    public Transform pantalla;

    private BossState currentState;
    public BossState CurrentState
    {
        get
        {
            return currentState;
        }
        set
        {
            if (flashInProcess)
            {
                myMesh.material.SetColor("EmissiveCol", Color.black);
                flashInProcess = false;
            }
            StopAllCoroutines();
            currentState = value;
            for(int i=0; i<turrets.Length; ++i)
            {
                turrets[i].Pattern = ShootingPattern.styleToPattern(currentState.turretStyle[i]);
                turrets[i].bulletMovementType = currentState.bulletStyle[i];
                                
                turrets[i].Deploy(currentState.turretStyle[i] != ShootingPattern.ShootingStyle.None);
            }

            StartCoroutine(setTurretsState(false, currentState.activityTime));
        }
    }

    IEnumerator setTurretsState(bool state, float time)
    {
        yield return new WaitForSeconds(time);
        if (HP > 0)
        {
            foreach (TurretCoil_Behaviour tc in turrets)
            {
                if (state)
                {
                    tc.startShooting();
                }
                else
                {
                    tc.stopShooting();
                }
            }
            StartCoroutine(setTurretsState(!state, !state ? currentState.recoverTime : currentState.activityTime));
        }
    }

    bool musicStarted = false;
    private void Start()
    {
        AudioManager.instance.StopAll();
        currentState = new BossState();
        if (!startPassive)
        {
            Invoke(nameof(checkStateChange), 0.2f);

            musicStarted = true;
            AudioManager.instance.Play("boss2");
            AudioManager.instance.PlayMuted("boss3");
        }
        else
        {
            foreach (TurretCoil_Behaviour tc in turrets)
            {
                tc.Deploy(false);
            }
        }

    }

    private void Update()
    {
        if (Application.isEditor)
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                Harm(33);
            }
        }
    }

    public void OnCollisionEnterChild(Collision collision)
    {
        if (collision.gameObject.CompareTag("Disk"))
            Harm(damageTaken);
    }

    bool modoDiablo = false;
    void Harm(int damage= 2)
    {
        if (HP > 0)
        {
            if (!musicStarted)
            {
                musicStarted = true;
                AudioManager.instance.Play("boss2");
                AudioManager.instance.PlayMuted("boss3");
            }
            HP -= damage;
            ScreenShakeCall.instance.ShakeCamera(2, .25f);
            if (HP > 0)
                checkStateChange();
            else
                Die();
            StartCoroutine(bodyFlash());
            harmEv.Invoke();

            if (HP < 35 && !modoDiablo)
            {
                modoDiablo = true;
                AudioManager.instance.FadeMutedIn("boss3", 2, 100);
            }
        }
    }
    bool flashInProcess = false;
    IEnumerator bodyFlash()
    {
        flashInProcess = true;
        myMesh.material.SetColor("EmissiveCol", Color.white * .75f);
        yield return new WaitForSeconds(0.1f);
        flashInProcess = false;
        myMesh.material.SetColor("EmissiveCol", Color.black);
    }

    void Die()
    {
        GameMaster.Instance.player.GetComponent<PlayerController>().invencible = true;
        StartCoroutine(keepSpawningExplosions(.15f));
        dieEv.Invoke();
        LeanTween.color(sheenImage.GetComponent<RectTransform>(), Color.white, 3).setOnComplete(resetSheen).setDelay(.5f);
        Invoke(nameof(wipeAllBullets), 3.5f);
        GameMaster.Instance.player.GetComponent<PlayerController>().bossKilled = true;

        StartCoroutine(AudioManager.instance.FadeOut("boss2", 1));
        StartCoroutine(AudioManager.instance.FadeOut("boss3", 1));
        Invoke(nameof(StopAllSongs), 1);
    }

    void StopAllSongs()
    {
        AudioManager.instance.StopAll();
    }

    IEnumerator keepSpawningExplosions(float t)
    {
        spawnExplosion();
        yield return new WaitForSeconds(t);
        StartCoroutine(keepSpawningExplosions(t));
    }

    void spawnExplosion()
    {
        ObjectPooler.instance.SpawnFromPool("BossExplosion", pantalla.position+new Vector3(Random.Range(-2, 2), Random.Range(-1.5f, 1.5f), 0));

        ScreenShakeCall.instance.ShakeCamera(3, .25f);
    }

    void wipeAllBullets()
    {
        foreach (BulletMovement bm in FindObjectsOfType<BulletMovement>())
        {
            Destroy(bm.gameObject);
        }
    }

    void resetSheen()
    {
        StopCoroutine(keepSpawningExplosions(.5f));
        foreach (TurretCoil_Behaviour tc in turrets)
        {
            tc.stopShooting();
            tc.Deploy(false);
        }
        LeanTween.color(sheenImage.GetComponent<RectTransform>(), new Color(1, 1, 1, 0), .15f).setDelay(.5f);
        Invoke(nameof(playerWin), 2);
    }
    void playerWin()
    {
        GameMaster.Instance.player.GetComponent<PlayerController>().Win();
    }


    void checkStateChange()
    {
        BossState auxNewState = null;
        foreach(BossState bs in states)
        {
            if (auxNewState == null || (bs.healthActivation>=HP && !bs.used))
            {
                auxNewState = bs;
            }
        }

        if (auxNewState != currentState)
        {
            if(currentState!=null)
                currentState.used = true;
            CurrentState = auxNewState;
        }
    }
}
