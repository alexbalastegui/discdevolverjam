﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletMovement : MonoBehaviour
{
    public enum BulletMovementType
    {
        Linear, ZigZag, Wave
    }

    [Header("General settings")]
    public BulletMovementType movementType;
    public int damage = 1;
    public float speed=1;
    public float lifetime = 5;

    //Wave data
    [Header("Wave settings")]
    [DrawIf("movementType", BulletMovementType.Linear, DrawIfAttribute.DisablingType.ReadOnly, DrawIfAttribute.Comparator.NotEqual)]
    public float waveLength = 1;
    [DrawIf("movementType", BulletMovementType.Linear, DrawIfAttribute.DisablingType.ReadOnly, DrawIfAttribute.Comparator.NotEqual)]
    public float waveAmplitude = 1;

    [Space(3)]
    public GameObject father = null;

    [Header("Materials")]
    public Material regularMat;
    public Material waveMat;
    public Material zigzagMat;

    [Header("VFX")]
    public GameObject explosionParticle;

    private float timestamp = 0;
    private Vector3 lastPosition;
    private Vector3 startPosition;
    
    private void setUp()
    {
        lastPosition = transform.position;
        startPosition = transform.position;

        gameObject.LeanScale(transform.localScale, .75f).setFrom(Vector3.zero).setEaseOutElastic();
        timestamp = Time.time;

        switch (movementType)
        {
            case BulletMovementType.Linear:
                GetComponent<ParticleSystemRenderer>().material = regularMat;
                break;
            case BulletMovementType.Wave:
                GetComponent<ParticleSystemRenderer>().material = waveMat;
                break;
            case BulletMovementType.ZigZag:
                GetComponent<ParticleSystemRenderer>().material = zigzagMat;
                break;
        }

        CancelInvoke();
        InvokeRepeating(nameof(hitScan), 0, .25f);
        Invoke(nameof(shutDown), lifetime);
    }
    private void Start()
    {
        setUp();
    }

    private void OnEnable()
    {
        setUp();
    }
    
    void Update()
    {
        Vector3 frameOffset = Vector3.zero;
        frameOffset += transform.position+transform.forward * speed * Time.deltaTime;

        if (movementType == BulletMovementType.Wave)
        {
            float sinFunc = Mathf.Asin(Mathf.Sin((Time.time - timestamp) * 1 / waveLength + Mathf.PI*.67f)) * waveAmplitude * Time.deltaTime;
            frameOffset += sinFunc * transform.right;
        }
        else if (movementType == BulletMovementType.ZigZag)
        {
            float sinFunc = (Utils.step(Mathf.Sin((Time.time - timestamp) * 1 / waveLength + Mathf.PI*.67f ), 0)*2-1)*waveAmplitude*Time.deltaTime;
            frameOffset += sinFunc * transform.right;
        }


        transform.position = frameOffset;
    }

    void hitScan()
    {
        RaycastHit[] rayHits=Physics.RaycastAll(lastPosition, (transform.position - lastPosition), (transform.position - lastPosition).magnitude, ~LayerMask.GetMask("Bullet"));
        Debug.DrawLine(lastPosition, transform.position, Color.magenta, .1f);

        foreach (RaycastHit rh in rayHits)
        {
            if (rh.collider != null && !rh.collider.isTrigger && rh.collider.gameObject!=gameObject && !rh.collider.gameObject.CompareTag("Player"))
                bulletHit();
        }

        lastPosition = transform.position;
    }

    public int bulletHit()
    {
        GetComponent<ParticleSystem>().Stop();
        ObjectPooler.instance.SpawnFromPool("BulletExplosion", transform.position);
        Invoke(nameof(shutDown), .1f);
        return damage;
    }
    void shutDown()
    {
        if (GameMaster.Instance.Pooler != null)
            gameObject.SetActive(false);
        else
            Destroy(gameObject);
    }
}
