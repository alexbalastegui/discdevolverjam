﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ShootingPattern
{
    public enum ShootingStyle
    {
        None, All, Cross, Cardinal, CW_Loop, CCW_Loop, ChrisCross
    }

    public ShootingPattern(float _initialDelay, float _repeatDelay)
    {
        initialDelay = _initialDelay;
        repeatDelay = _repeatDelay;
    }

    public static ShootingPattern operator *(ShootingPattern a, float b) => new ShootingPattern(a.initialDelay * b, a.repeatDelay * b);

    public float initialDelay = 0;
    public float repeatDelay = 1;
        
    public static readonly ShootingPattern[] chrissCross = new ShootingPattern[8] {
                                                                        new ShootingPattern(0, 1),
                                                                        new ShootingPattern(.5f, 1),
                                                                        new ShootingPattern(0, 1),
                                                                        new ShootingPattern(.5f, 1),
                                                                        new ShootingPattern(0, 1),
                                                                        new ShootingPattern(.5f, 1),
                                                                        new ShootingPattern(0, 1),
                                                                        new ShootingPattern(.5f, 1)};
    public static readonly ShootingPattern[] cross = new ShootingPattern[8] {
                                                                        new ShootingPattern(0, 0),
                                                                        new ShootingPattern(0, 1),
                                                                        new ShootingPattern(0, 0),
                                                                        new ShootingPattern(0, 1),
                                                                        new ShootingPattern(0, 0),
                                                                        new ShootingPattern(0, 1),
                                                                        new ShootingPattern(0, 0),
                                                                        new ShootingPattern(0, 1)};
    public static readonly ShootingPattern[] cardinal = new ShootingPattern[8] {
                                                                        new ShootingPattern(0, 1),
                                                                        new ShootingPattern(0, 0),
                                                                        new ShootingPattern(0, 1),
                                                                        new ShootingPattern(0, 0),
                                                                        new ShootingPattern(0, 1),
                                                                        new ShootingPattern(0, 0),
                                                                        new ShootingPattern(0, 1),
                                                                        new ShootingPattern(0, 0)};
    public static readonly ShootingPattern[] ccwLoop = new ShootingPattern[8] {
                                                                        new ShootingPattern(0, 1),
                                                                        new ShootingPattern(0.125f, 1),
                                                                        new ShootingPattern(0.25f, 1),
                                                                        new ShootingPattern(0.375f, 1),
                                                                        new ShootingPattern(0.5f, 1),
                                                                        new ShootingPattern(0.625f, 1),
                                                                        new ShootingPattern(0.75f, 1),
                                                                        new ShootingPattern(0.875f, 1)};
    public static readonly ShootingPattern[] cwLoop = new ShootingPattern[8] {
                                                                        new ShootingPattern(0, 1),
                                                                        new ShootingPattern(0.875f, 1),
                                                                        new ShootingPattern(0.75f, 1),
                                                                        new ShootingPattern(0.625f, 1),
                                                                        new ShootingPattern(0.5f, 1),
                                                                        new ShootingPattern(0.375f, 1),
                                                                        new ShootingPattern(0.25f, 1),
                                                                        new ShootingPattern(0.125f, 1)};
    public static readonly ShootingPattern[] all = new ShootingPattern[8] {
                                                                        new ShootingPattern(0, 1),
                                                                        new ShootingPattern(0, 1),
                                                                        new ShootingPattern(0, 1),
                                                                        new ShootingPattern(0, 1),
                                                                        new ShootingPattern(0, 1),
                                                                        new ShootingPattern(0, 1),
                                                                        new ShootingPattern(0, 1),
                                                                        new ShootingPattern(0, 1)};
    public static readonly ShootingPattern[] none = new ShootingPattern[8] {
                                                                        new ShootingPattern(0, 0),
                                                                        new ShootingPattern(0, 0),
                                                                        new ShootingPattern(0, 0),
                                                                        new ShootingPattern(0, 0),
                                                                        new ShootingPattern(0, 0),
                                                                        new ShootingPattern(0, 0),
                                                                        new ShootingPattern(0, 0),
                                                                        new ShootingPattern(0, 0)};

    public static ShootingPattern[] styleToPattern(ShootingStyle a)
    {
        switch (a)
        {
            default:
            case ShootingStyle.None:
                return (ShootingPattern[])ShootingPattern.none.Clone();
            case ShootingStyle.All:
                return (ShootingPattern[])ShootingPattern.all.Clone();
            case ShootingStyle.Cross:
                return (ShootingPattern[])ShootingPattern.cross.Clone();
            case ShootingStyle.Cardinal:
                return (ShootingPattern[])ShootingPattern.cardinal.Clone();
            case ShootingStyle.CW_Loop:
                return (ShootingPattern[])ShootingPattern.cwLoop.Clone();
            case ShootingStyle.CCW_Loop:
                return (ShootingPattern[])ShootingPattern.ccwLoop.Clone();
            case ShootingStyle.ChrisCross:
                return (ShootingPattern[])ShootingPattern.chrissCross.Clone();
        }
    }
}

public class TurretCoil_Behaviour : MonoBehaviour
{
    [Header("Necesary fields")]
    public GameObject bulletPrefab;
    public string bulletKey="Bullet";
    public Transform[] shootingSockets = new Transform[8];
    
    [Header("Turret settings")]
    public ShootingPattern.ShootingStyle shootingStyle = ShootingPattern.ShootingStyle.ChrisCross;
    public float shootFrequency = 1;

    [Header("Bullet settings")]
    public float bulletSpeed = 3;
    private float bulletSpawnOffset=.15f;
    public BulletMovement.BulletMovementType bulletMovementType= BulletMovement.BulletMovementType.Linear;
    [DrawIf("bulletMovementType", BulletMovement.BulletMovementType.Linear, DrawIfAttribute.DisablingType.ReadOnly, DrawIfAttribute.Comparator.NotEqual)]
    public float waveLength = 1;
    [DrawIf("bulletMovementType", BulletMovement.BulletMovementType.Linear, DrawIfAttribute.DisablingType.ReadOnly, DrawIfAttribute.Comparator.NotEqual)]
    public float waveAmplitude = 1;

    [Header("VFX")]
    public GameObject muzzle;

    private ShootingPattern[] pattern = new ShootingPattern[8];
    public ShootingPattern[] Pattern
    {
        get
        {
            return pattern;
        }
        set
        {
            pattern = value;
            if (pattern != ShootingPattern.styleToPattern(ShootingPattern.ShootingStyle.None))
            {
                for (int i = 0; i < pattern.Length; ++i)
                {
                    pattern[i] = pattern[i] * (1f / shootFrequency);
                }
                Invoke(nameof(startShooting), .5f);
            }
        }
    }

    private Material mat;
    
    private void Awake()
    {
        initialHeight = transform.position.y;
        mat = GetComponentInChildren<MeshRenderer>().material;
        DeployHard(false);
    }

    public float initialHeight;
    private void Start()
    {
        if(FindObjectOfType<BossBehaviour>()==null)
            Deploy(true);
 
    
        Pattern = ShootingPattern.styleToPattern(shootingStyle);
    }

    private float height = .51f;
    private bool deployed = true;
    [ContextMenu("Deploy_Conceal")]
    public void Deploy(bool newState)
    {
        if (deployed != newState)
        {
            deployed = newState;
            transform.LeanMoveLocalY(!deployed ? -height : initialHeight, .5f).setFrom(transform.position.y).setEaseInOutBack();
            if (!deployed)
                stopShooting();
        }
    }
    public void DeployHard(bool newState)
    {
        if (deployed != newState)
        {
            deployed = newState;
            transform.position = new Vector3(transform.position.x, (!deployed ? -height : initialHeight), transform.position.z);
        }
    }
    private void shoot(int socket)
    {
        StartCoroutine(updateLEDshader(0.25f, socket, 1));
        
        GameObject bullet;
        if (GameMaster.Instance.Pooler != null && GameMaster.Instance.Pooler.poolDictionary.ContainsKey("Bullet"))
            bullet=GameMaster.Instance.Pooler.SpawnFromPool(bulletKey, shootingSockets[socket].position+shootingSockets[socket].forward*bulletSpawnOffset, shootingSockets[socket].rotation);
        else
            bullet=Instantiate(bulletPrefab, shootingSockets[socket].position, shootingSockets[socket].rotation);
        bullet.GetComponent<BulletMovement>().speed = bulletSpeed;
        bullet.GetComponent<BulletMovement>().father = gameObject;
        bullet.GetComponent<BulletMovement>().movementType = bulletMovementType;
        //wave
        bullet.GetComponent<BulletMovement>().waveAmplitude = waveAmplitude;
        bullet.GetComponent<BulletMovement>().waveLength = waveLength;

        Instantiate(muzzle, shootingSockets[socket].position, shootingSockets[socket].rotation);
    }

    private IEnumerator updateLEDshader(float time, int socket, float glow)
    {
        mat.SetFloat("LED_"+(socket+1).ToString()+"_Glow", glow);
        yield return new WaitForSeconds(time);
        mat.SetFloat("LED_" + (socket + 1).ToString() + "_Glow", 0);
    }

    public void stopShooting()
    {
        StopAllCoroutines();

        for(int i=0; i<shootingSockets.Length; ++i)
        {
            mat.SetFloat("LED_" + (i + 1).ToString() + "_Glow", 0);
        }
        //StopCoroutine(shootFromPos(0, 0));
        //StopCoroutine(shootFromPos(0, 0, 0));
    }

    private bool canStartShooting = true;
    private void reenableCanStartShooting()
    {
        canStartShooting = true;
    }
    public void startShooting()
    {
        if (canStartShooting)
        {
            StopAllCoroutines();
            if (shootFrequency != 0)
            {
                for (int i = 0; i < pattern.Length; ++i)
                {
                    StartCoroutine(shootFromPos(i, pattern[i].initialDelay, pattern[i].repeatDelay));
                }
            }

            canStartShooting = false;
            Invoke(nameof(reenableCanStartShooting), .35f);
        }
    }

    IEnumerator shootFromPos(int index, float startDelay, float repeatDelay)
    {
        if (!(startDelay == 0 && repeatDelay == 0))
        {
            yield return new WaitForSeconds(startDelay);
            shoot(index);
            StartCoroutine(shootFromPos(index, repeatDelay));
        }
    }
    IEnumerator shootFromPos(int index, float repeatDelay)
    {
        yield return new WaitForSeconds(repeatDelay);
        shoot(index);
        StartCoroutine(shootFromPos(index, repeatDelay));
    }

}
