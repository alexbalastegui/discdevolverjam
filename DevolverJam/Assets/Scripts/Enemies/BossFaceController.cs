﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Expression
{
    Idle, Talk, Angry, Laugh
}

public class BossFaceController : MonoBehaviour
{
    public BossBehaviour boss;

    [MinMaxCustomSlider]
    public ValueRange moodChangeTime;

    public Expression startExpression;

    public Texture Idle;
    public Texture IdleGlitch;
    public Texture Angry;
    public Texture AngryGlitch;
    public Texture Talk;
    public Texture TalkGlitch;
    public Texture Laugh;
    public Texture Damage;

    private Material mat;
    private Texture Tex
    {
        set
        {
            mat.SetTexture("_FaceTexture", value);
            if (value == Talk || value == TalkGlitch)
            {
                mat.SetFloat("_Cols", 10);
                StartCoroutine(playQuote());
            }
            else
                mat.SetFloat("_Cols", 5);

            currentTex = value;
        }
    }

    private Texture currentTex;

    private float MoodDelay
    {
        get
        {
            return Random.Range(moodChangeTime.MinValue, moodChangeTime.MaxValue);
        }
    }

    Texture RegularToGlitch(Texture _tex)
    {
        if (_tex == Idle)
            return IdleGlitch;
        else if (_tex == Angry)
            return AngryGlitch;
        else if (_tex == Talk)
            return TalkGlitch;
        else
        {
            Debug.LogError("Unexpected texture at RegularToGlitch method in BossFaceController.cs");
            return null;
        }

    }
    Texture ExpressionToTexture(Expression _exp)
    {
        switch (_exp)
        {
            default:
            case Expression.Idle:
                return Idle;
            case Expression.Angry:
                return Angry;
            case Expression.Laugh:
                return Laugh;
            case Expression.Talk:
                return Talk;
        }
    }

    Texture GlitchIfNeeded(Texture tex)
    {
        return (Random.value * 100 > boss.HP) ? RegularToGlitch(tex) : tex;
    }

    string PlayRandomQuote()
    {
        string name = null;
        switch((int)Random.Range(0, 4))
        {
            case 0:
                name = "steve4";
                break;
            case 1:
                name = "steve5";
                break;
            case 2:
                name = "steve6";
                break;
            case 3:
                name = "steve8";
                break;
        }

        AudioManager.instance.Play(name);
        return name;
    }
    void StopAllQuotes()
    {
        StopCoroutine(playQuote());
        AudioManager.instance.Stop("steve4");
        AudioManager.instance.Stop("steve5");
        AudioManager.instance.Stop("steve6");
        AudioManager.instance.Stop("steve8");
    }

    // Start is called before the first frame update
    void Start()
    {
        mat = GetComponent<SkinnedMeshRenderer>().material;
        boss.harmEv.AddListener(setDamagedFace);
        GameMaster.Instance.player.GetComponent<PlayerController>().damageEv.AddListener(setLaughFace);

        Tex = ExpressionToTexture(startExpression);
    }

    void decideTexture()
    {
        StopAllQuotes();

        //Idling
        int idlingTexture = Random.Range(0, 3);

        if (((currentTex == Idle || currentTex == IdleGlitch) && idlingTexture == 0) ||
            ((currentTex == Angry || currentTex == AngryGlitch) && idlingTexture == 1) ||
            ((currentTex == Talk || currentTex == TalkGlitch) && idlingTexture == 2))
            idlingTexture = (++idlingTexture % 3);

        switch (idlingTexture)
        {
            case 0:
                Tex = GlitchIfNeeded(Idle);
                break;
            case 1:
                Tex = GlitchIfNeeded(Angry);
                break;
            case 2:
                Tex = GlitchIfNeeded(Talk);
                break;
        }
        

        if (boss.HP > 0)
            Invoke(nameof(decideTexture), MoodDelay);
    }

    void setDamagedFace()
    {
        CancelInvoke(nameof(decideTexture));
        StopAllQuotes();

        if (boss.HP > 0)
        {
            Tex = Damage;
            mat.SetFloat("_Index", 1);

            Invoke(nameof(decideTexture), .45f);
        }
        else
        {
            Tex = Angry;
            AudioManager.instance.PlayOneShot("bossDie");
        }
    }

    void setLaughFace()
    {
        if (boss.HP > 0)
        {
            CancelInvoke(nameof(decideTexture));

            Tex = Laugh;

            Invoke(nameof(decideTexture), .5f);
        }
    }
    
    IEnumerator playQuote()
    {
        float delay = 0;
        switch (PlayRandomQuote())
        {
            default:
            case "steve4":
                delay = 1.8f;
                break;
            case "steve5":
                delay = 2.7f;
                break;
            case "steve6":
                delay = 2.3f;
                break;
            case "steve8":
                delay = 2.9f;
                break;
        }
        yield return new WaitForSeconds(delay);
        Tex = Idle;
        //StartCoroutine(playQuotes());
    }
}
