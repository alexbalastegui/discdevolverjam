﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class FolderController : MonoBehaviour, IPooledObject{

    [SerializeField] Animator anim = default;
    [SerializeField] Transform container = default;
    Rigidbody rb;
    [SerializeField] float speed = 2;
    PlayerController player;

    [Space]
    [SerializeField, Range(0, 15)] float detectionDistance = 10;
    [SerializeField] LayerMask whatIsPlayer = 8;
    bool playerDetected;

    bool patroling;
    bool rotationSelected;

    [Space]
    [SerializeField, Range(0, 2)] float movementRegainTime = 1f;
    [SerializeField, Range(0, 10)] float attackForce = 5f;
    bool walking;
    float currentTime;
    Vector3 force;
    Vector3 posToGo;

    NavMeshAgent agent;

    [HideInInspector] public EnemySpawner spawner;
    [HideInInspector] public RoomTrigger room;

    [Space]
    [SerializeField] int percentageAmmount = 1;

    // Start is called before the first frame update
    void Awake(){
        agent = GetComponent<NavMeshAgent>();
        rb = GetComponent<Rigidbody>();
        player = FindObjectOfType<PlayerController>();
        agent.speed = speed;
    }

    public void OnObjectSpawn(){
        rb.isKinematic = true;
        playerDetected = patroling = rotationSelected = walking = false;
        currentTime = 0;
        force = posToGo = Vector3.zero;
        agent.Warp(transform.position);
    }

    void Update(){
        if(!playerDetected && Physics.CheckSphere(transform.position, detectionDistance, whatIsPlayer)){
            rb.isKinematic = false;
            playerDetected = true;
            walking = true;
        }

        if (playerDetected){
            if (walking){
                agent.SetDestination(player.transform.position);
                agent.isStopped = false;
            }else{
                currentTime += 1 / movementRegainTime * Time.deltaTime;
                container.transform.localPosition = new Vector3(0, 0, 0);
                rb.velocity = Vector3.Lerp(force, Vector3.zero, currentTime);
            }
        }else{
            if (!patroling && !rotationSelected){
                posToGo = transform.position + new Vector3(Random.Range(-5f, 5f), 0, Random.Range(-5f, 5f));
                StartCoroutine(Patrol());
                rotationSelected = true;
                patroling = true;
            }
        }
    }

    void FixedUpdate(){
        if(walking && playerDetected){
            rb.velocity = Vector3.zero;
        }
    }

    IEnumerator Patrol(){
        agent.isStopped = false;
        agent.SetDestination(posToGo);
        yield return new WaitForSeconds(Random.Range(1f, 2f));
        rb.velocity = Vector3.zero;
        patroling = false;
        rotationSelected = false;
        agent.isStopped = !playerDetected;
    }

    void OnCollisionEnter(Collision col){
        if (col.gameObject.CompareTag("Player")){
            anim.SetTrigger("Hit");
            walking = false;
            currentTime = 0;
            agent.isStopped = true;
            rb.velocity = -container.forward * attackForce;
            force = rb.velocity;
            StartCoroutine(RegainMobility());
            player.Damage(1);
        }

        if (col.gameObject.CompareTag("Disk")){
            gameObject.SetActive(false);
            room.Die(gameObject);
            GameMaster.Instance.Pooler.SpawnFromPool("EnemyExplosion", transform.position);
            if (Utils.spawnDisk())
                GameMaster.Instance.Pooler.SpawnFromPool("DiskPickup", transform.position);
            player.percentage += percentageAmmount;
            col.gameObject.GetComponent<Rigidbody>().velocity = Vector3.zero;
        }
    }

    void OnTriggerEnter(Collider col){
        if (col.CompareTag("Disk")){
            gameObject.SetActive(false);
            room.Die(gameObject);
            GameMaster.Instance.Pooler.SpawnFromPool("EnemyExplosion", transform.position);
            if (Utils.spawnDisk())
                GameMaster.Instance.Pooler.SpawnFromPool("DiskPickup", transform.position);
            player.percentage += percentageAmmount;
            col.GetComponent<Rigidbody>().velocity = Vector3.zero;
        }
    }

    IEnumerator RegainMobility(){
        yield return new WaitForSeconds(movementRegainTime);
        rb.velocity = Vector3.zero;
        walking = true;
    }

    void OnDrawGizmosSelected(){
        Gizmos.DrawWireSphere(transform.position, detectionDistance);
    }
}
