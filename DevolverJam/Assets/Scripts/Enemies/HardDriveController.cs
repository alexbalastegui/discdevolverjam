﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class HardDriveController : MonoBehaviour, IPooledObject{

    [SerializeField, Min(0)] float speed = 2f;

    [Space]
    [SerializeField, Range(0, 10)] float playerDetectionRange = 3.5f;
    [SerializeField] LayerMask whatIsPlayer = 8;
    [SerializeField] LayerMask whatIsWall = -1;
    PlayerController player;
    bool followingPlayer;

    bool walking;
    bool rotationSelected;

    [Header("Attack")]
    [SerializeField, Min(0)] Vector3 playerHitRange = Vector3.one;
    [SerializeField] Transform playerHitPos = default;
    bool attacking;
    [SerializeField, Range(0, 1)] float particleDelay = .25f;
    [SerializeField, Range(0, 2)] float attackDuration = .5f;

    [Space]
    [SerializeField] Animator anim = default;
    [SerializeField, Range(1, 5)] int damageAmount = 2;

    NavMeshAgent agent;
    Vector3 posToGo;

    [HideInInspector] public EnemySpawner spawner;
    [HideInInspector] public RoomTrigger room;

    [SerializeField] Transform explosionPos = default;
    [SerializeField, Range(0, 1)] float disappearDelay = .5f;

    [Space]
    [SerializeField, Range(0, 5)] float explosionRange;

    void Awake(){
        player = FindObjectOfType<PlayerController>();
        agent = GetComponent<NavMeshAgent>();
        agent.speed = speed;
    }

    public void OnObjectSpawn(){
        walking = rotationSelected = followingPlayer = attacking = false;
        posToGo = transform.position;
        agent.Warp(transform.position);
        agent.isStopped = true;
    }

    void Update(){
        if (Physics.CheckSphere(transform.position, playerDetectionRange, whatIsPlayer) && !followingPlayer){
            followingPlayer = true;
        }

        if (followingPlayer && !attacking){
            if (Physics.CheckBox(playerHitPos.position, playerHitRange / 2, playerHitPos.rotation, whatIsPlayer)){
                attacking = true;
                agent.isStopped = true;
                StartCoroutine(Attack());
            }else{
                agent.isStopped = false;
                agent.SetDestination(player.transform.position);
            }
        }else if (!attacking){
            if (!walking && !rotationSelected){
                posToGo = transform.position + new Vector3(Random.Range(-5f, 5f), 0, Random.Range(-5f, 5f));
                StartCoroutine(Patrol());
                walking = true;
                rotationSelected = true;
            }else if (!Physics.CheckBox(playerHitPos.position, playerHitRange / 2, playerHitPos.rotation, whatIsWall)){
                agent.isStopped = false;
            }
        }
    }

    IEnumerator Patrol(){
        agent.isStopped = false;
        agent.SetDestination(posToGo);
        yield return new WaitForSeconds(Random.Range(1f, 2f));
        walking = false;
        rotationSelected = false;
        agent.isStopped = !followingPlayer;
    }

    IEnumerator Attack(){
        anim.SetTrigger("Attack");
        yield return new WaitForSeconds(particleDelay);
        ObjectPooler.instance.SpawnFromPool("HDDExplosion", explosionPos.position);
        AudioManager.instance.PlayOneShot("electro1");
        AudioManager.instance.PlayOneShot("explo1");
        yield return new WaitForSeconds(attackDuration - particleDelay);
        if (Physics.CheckBox(playerHitPos.position, playerHitRange / 2, playerHitPos.rotation, whatIsPlayer)){
            player.Damage(damageAmount);
        }
        yield return new WaitForSeconds(disappearDelay);
        if(Physics.CheckSphere(explosionPos.position, explosionRange, whatIsPlayer)){
            player.Damage(1);
        }
        player.percentage += Utils.RAM_cleanseSmall;
        room.Die(gameObject);
        if (Utils.spawnDisk())
            GameMaster.Instance.Pooler.SpawnFromPool("DiskPickup", transform.position);
        gameObject.SetActive(false);
    }

    void OnDrawGizmosSelected(){
        Gizmos.DrawWireSphere(transform.position, playerDetectionRange);
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(explosionPos.position, explosionRange);
    }

    void OnDrawGizmos(){
        Gizmos.color = Color.red;
        if (playerHitPos != null){
            Gizmos.DrawWireCube(playerHitPos.position, playerHitRange);
        }
    }
}
