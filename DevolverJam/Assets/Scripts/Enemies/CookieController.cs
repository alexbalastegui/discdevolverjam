﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CookieController : MonoBehaviour, IPooledObject{

    [SerializeField] float speed = 5f;
    [SerializeField, Range(0, 5)] float timeToAttack = 2f;

    bool canMove;
    PlayerController player;
    Rigidbody rb;

    [HideInInspector] public EnemySpawner spawner;
    [HideInInspector] public RoomTrigger room;

    // Start is called before the first frame update
    void Awake(){
        player = FindObjectOfType<PlayerController>();
        rb = GetComponent<Rigidbody>();
    }

    public void OnObjectSpawn(){
        StartCoroutine(TimeToAttack());
        canMove = false;
    }

    // Update is called once per frame
    void FixedUpdate(){
        if (canMove)
            rb.MovePosition(rb.position + transform.forward * speed * Time.fixedDeltaTime);
    }

    void OnCollisionEnter(Collision col){
        if (col.gameObject.CompareTag("Wall")){
            transform.LookAt(new Vector3(player.transform.position.x, transform.position.y, player.transform.position.z));
        }

        if (col.gameObject.CompareTag("Disk")){
            gameObject.SetActive(false);
            room.Die(gameObject);
            GameMaster.Instance.Pooler.SpawnFromPool("EnemyExplosion", transform.position);
            if(Utils.spawnDisk())
                GameMaster.Instance.Pooler.SpawnFromPool("DiskPickup", transform.position);
            col.gameObject.GetComponent<Rigidbody>().velocity = Vector3.zero;
        }
    }

    void OnTriggerEnter(Collider col){
        if (col.CompareTag("Disk")){
            gameObject.SetActive(false);
            room.Die(gameObject);
            GameMaster.Instance.Pooler.SpawnFromPool("EnemyExplosion", transform.position);
            if (Utils.spawnDisk())
                GameMaster.Instance.Pooler.SpawnFromPool("DiskPickup", transform.position);
            col.GetComponent<Rigidbody>().velocity = Vector3.zero;
        }

        if (col.gameObject.CompareTag("Player")){
            player.Damage(1);
        }
    }

    IEnumerator TimeToAttack(){
        yield return new WaitForSeconds(timeToAttack);
        transform.LookAt(new Vector3(player.transform.position.x, transform.position.y, player.transform.position.z));
        canMove = true;
    }
}
