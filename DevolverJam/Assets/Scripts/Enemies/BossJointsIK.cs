﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteAlways]
public class BossJointsIK : MonoBehaviour
{
    public float angleThreshold = 0;
    public Transform joint1;
    public Transform joint2;
    public Transform endEffector;

    // Update is called once per frame
    void Update()
    {
        joint2.localEulerAngles = new Vector3(0, 0, 180-angleThreshold)-new Vector3(joint1.localEulerAngles.x, joint1.localEulerAngles.x, 
           Utils.remap(Mathf.Abs(joint1.localEulerAngles.z), 0, 90, angleThreshold, 180))+new Vector3(.066f, 3.421f, 0);
        endEffector.rotation = Quaternion.LookRotation(Vector3.forward, Vector3.up)*Quaternion.Euler(new Vector3(0, 90, 0));
        endEffector.localEulerAngles += new Vector3(1.066f, 3.421f, 0);
    }
}
