﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyFrisbie : MonoBehaviour, IPooledObject{

    [SerializeField, Range(0, 5f)] float damageVelocityThreshold = 1.5f;

    [Space]
    [SerializeField] float speed = 10;
    Rigidbody rb;
    [SerializeField] Transform frisbie = default;
    float heightVelocity;
    float velocity;

    PlayerController player;

    void Awake(){
        rb = GetComponent<Rigidbody>();
        player = FindObjectOfType<PlayerController>();
    }

    // Start is called before the first frame update
    public void OnObjectSpawn(){
        rb.velocity = Vector3.zero;
        rb.AddForce(transform.forward.normalized * speed, ForceMode.Impulse);
    }

    void FixedUpdate(){
        velocity = rb.velocity.magnitude;

        float positionToGo = Mathf.SmoothDamp(frisbie.localPosition.y, -1 + velocity / speed, ref heightVelocity, .1f);
        frisbie.localPosition = new Vector3(0, positionToGo, 0);
    }

    void OnCollisionEnter(Collision col){
        if (col.gameObject.CompareTag("Player")){
            if(velocity > damageVelocityThreshold){
                player.Damage(1);
                gameObject.SetActive(false);
                ObjectPooler.instance.SpawnFromPool("EnemyDiskBreak", transform.position);
            }
        }
    }
}
