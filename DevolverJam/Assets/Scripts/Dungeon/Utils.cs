﻿#define _DEBUG

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Direction
{
    _TOP, _RIGHT, _BOTTOM, _LEFT, _MAX
}

public static class Utils
{
    public static Vector3 dirToVec(Direction direction)
    {
        switch (direction)
        {
            case Direction._TOP:
                return new Vector3(0, 0, 1);
            case Direction._RIGHT:
                return new Vector3(1, 0, 0);
            case Direction._BOTTOM:
                return new Vector3(0, 0, -1);
            case Direction._LEFT:
                return new Vector3(-1, 0, 0);
        }
        return Vector3.one;
    }

    public const float easyDifficultyK = 200;
    public const float mediumDifficultyK = 50;
    public const float hardDifficultyK = 5;

    public static float difficultyFormula(int levelNumber, float difficultyConstant)
    {
        return levelNumber / Mathf.Sqrt(levelNumber * levelNumber + difficultyConstant);
    }
    public static void Print(string text){
        #if _DEBUG
        Debug.Log(text);
        #endif
    }

    public static bool isOdd(int num)
    {
        return num % 2 == 0;
    }

    public static float step(float val, float edge)
    {
        return val >= edge ? 1 : 0;
    }
    public static float remap(float value, float from1, float to1, float from2, float to2)
    {
        return (value - from1) / (to1 - from1) * (to2 - from2) + from2;
    }

    public static bool spawnDisk()
    {
        int maxHp = 15;
        int currentHealth=GameMaster.Instance.player.GetComponent<PlayerController>().health.currentHp;

        float chance = Mathf.Min(.5f, 1-currentHealth / maxHp);

        return chance>Random.value;
    }

    public static int RAM_cleanseSmall = 5;
    public static int RAM_cleanseBig = 7;

    [System.Serializable]
    public struct DifficultyDebug
    {
        public bool hardEstablished;
        public float easyRate;
        public  float mediumRate;
        public float hardRate;
    }
    
}