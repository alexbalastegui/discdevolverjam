﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEngine.Events;

public class DungeonGen : MonoBehaviour
{
    public Material debug;
    [Header("General settings")]
    public int complexity = 4;
    private float roomOffset = 10;
    [Tooltip("The prefab for the room in which the player starts")]
    public GameObject startingRoom;
    [MinMaxCustomSlider]
    public ValueRange thresholdRoomAmount;

    [Header("Rooms prefabs")]
    public GameObject[] topRooms;
    public GameObject[] rightRooms;
    public GameObject[] bottomRooms;
    public GameObject[] leftRooms;
    [Header("Rooms database")]
    public GameObject[] rooms;

    [SerializeField]
    private List<GameObject> dungeon=new List<GameObject>();
    public List<GameObject> Dungeon
    {
        get
        {
            return dungeon;
        }
    }

    private GameObject entryRoom;
    public GameObject EntryRoom
    {
        get
        {
            return entryRoom;
        }
    }
    private GameObject exitRoom;
    public GameObject ExitRoom
    {
        get
        {
            return exitRoom;
        }
    }
    
    bool CanSpawnRoom(Room room, Direction dir)
    {
        bool ans = true;

        RaycastHit[] raycastHit = Physics.RaycastAll(room.transform.position, Utils.dirToVec(dir), roomOffset);
        foreach (RaycastHit r in raycastHit)
        {
            if (r.collider.gameObject != room.gameObject)
            {
                ans = false;
            }
        }
        return ans;
    }

    void addDungeonLevel()
    {

        List<GameObject> auxDungeon = new List<GameObject>();
        for (int i = 0; i < dungeon.Count; ++i)
        {
            if (dungeon[i].GetComponent<Room>().OpenEdges())
            {
                if (dungeon[i].GetComponent<Room>().top == Gate.open && CanSpawnRoom(dungeon[i].GetComponent<Room>(), Direction._TOP))
                {
                    auxDungeon.Add(Instantiate(bottomRooms[Random.Range(0, bottomRooms.Length)], dungeon[i].transform.position + new Vector3(0, 0, roomOffset), Quaternion.identity));
                    auxDungeon[auxDungeon.Count - 1].GetComponent<Room>().bottom = Gate.closed;
                    dungeon[i].GetComponent<Room>().top = Gate.closed;
                }
                if (dungeon[i].GetComponent<Room>().right == Gate.open && CanSpawnRoom(dungeon[i].GetComponent<Room>(), Direction._RIGHT))
                {
                    auxDungeon.Add(Instantiate(leftRooms[Random.Range(0, leftRooms.Length)], dungeon[i].transform.position + new Vector3(roomOffset, 0, 0), Quaternion.identity));
                    auxDungeon[auxDungeon.Count - 1].GetComponent<Room>().left = Gate.closed;
                    dungeon[i].GetComponent<Room>().right = Gate.closed;
                }
                if (dungeon[i].GetComponent<Room>().bottom == Gate.open && CanSpawnRoom(dungeon[i].GetComponent<Room>(), Direction._BOTTOM))
                {
                    auxDungeon.Add(Instantiate(topRooms[Random.Range(0, topRooms.Length)], dungeon[i].transform.position + new Vector3(0, 0, -roomOffset), Quaternion.identity));
                    auxDungeon[auxDungeon.Count - 1].GetComponent<Room>().top = Gate.closed;
                    dungeon[i].GetComponent<Room>().bottom = Gate.closed;
                }
                if (dungeon[i].GetComponent<Room>().left == Gate.open && CanSpawnRoom(dungeon[i].GetComponent<Room>(), Direction._LEFT))
                {
                    auxDungeon.Add(Instantiate(rightRooms[Random.Range(0, rightRooms.Length)], dungeon[i].transform.position + new Vector3(-roomOffset, 0, 0), Quaternion.identity));
                    auxDungeon[auxDungeon.Count - 1].GetComponent<Room>().right = Gate.closed;
                    dungeon[i].GetComponent<Room>().left = Gate.closed;
                }
            }
        }
        dungeon.AddRange(auxDungeon);
    }

    [ContextMenu("Spawn Dungeon")]
    void spawnDungeon()
    {
        dungeon.Clear();

        GameObject myTempRoom = Instantiate(startingRoom, Vector3.zero, Quaternion.identity);
        dungeon.Add(myTempRoom);
        entryRoom = myTempRoom;
        entryRoom.GetComponent<Room>().Difficulty = Difficulty.Special;
        //entryRoom.GetComponentInChildren<MeshRenderer>().material.SetColor("_Tint", new Color(0.7f, 0.7f, 1));

        for (int j = 0; j < complexity || dungeon.Count < thresholdRoomAmount.MinValue; ++j)
        {
            List<GameObject> auxDungeon = new List<GameObject>();
            for (int i = 0; i < dungeon.Count; ++i)
            {
                if (dungeon[i].GetComponent<Room>().OpenEdges())
                {
                    if (dungeon[i].GetComponent<Room>().top == Gate.open && CanSpawnRoom(dungeon[i].GetComponent<Room>(), Direction._TOP))
                    {
                        auxDungeon.Add(Instantiate(bottomRooms[Random.Range(0, bottomRooms.Length)], dungeon[i].transform.position + new Vector3(0, 0, roomOffset), Quaternion.identity));
                        auxDungeon[auxDungeon.Count - 1].GetComponent<Room>().bottom = Gate.closed;
                        dungeon[i].GetComponent<Room>().top = Gate.closed;
                    }
                    if (dungeon[i].GetComponent<Room>().right == Gate.open && CanSpawnRoom(dungeon[i].GetComponent<Room>(), Direction._RIGHT))
                    {
                        auxDungeon.Add(Instantiate(leftRooms[Random.Range(0, leftRooms.Length)], dungeon[i].transform.position + new Vector3(roomOffset, 0, 0), Quaternion.identity));
                        auxDungeon[auxDungeon.Count - 1].GetComponent<Room>().left = Gate.closed;
                        dungeon[i].GetComponent<Room>().right = Gate.closed;
                    }
                    if (dungeon[i].GetComponent<Room>().bottom == Gate.open && CanSpawnRoom(dungeon[i].GetComponent<Room>(), Direction._BOTTOM))
                    {
                        auxDungeon.Add(Instantiate(topRooms[Random.Range(0, topRooms.Length)], dungeon[i].transform.position + new Vector3(0, 0, -roomOffset), Quaternion.identity));
                        auxDungeon[auxDungeon.Count - 1].GetComponent<Room>().top = Gate.closed;
                        dungeon[i].GetComponent<Room>().bottom = Gate.closed;
                    }
                    if (dungeon[i].GetComponent<Room>().left == Gate.open && CanSpawnRoom(dungeon[i].GetComponent<Room>(), Direction._LEFT))
                    {
                        auxDungeon.Add(Instantiate(rightRooms[Random.Range(0, rightRooms.Length)], dungeon[i].transform.position + new Vector3(-roomOffset, 0, 0), Quaternion.identity));
                        auxDungeon[auxDungeon.Count - 1].GetComponent<Room>().right = Gate.closed;
                        dungeon[i].GetComponent<Room>().left = Gate.closed;
                    }
                }
                if (dungeon.Count + auxDungeon.Count >= thresholdRoomAmount.MaxValue-1)
                    break;
            }
            dungeon.AddRange(auxDungeon);

            if (j > 50)
                break;
        }

        fixSiblingsConnections();
        fixOpenEnds();
        setExitRoom();
        updateRooms();
        DifficultyEstablisher.instance.SetDifficulty();
        DungeonFiller.instance.Fill();
    }
    
    void fixSiblingsConnections()
    {
        for (int i = 0; i < dungeon.Count; ++i)
        { //iters through all rooms and checks for connections between siblings
            if (dungeon[i].GetComponent<Room>().OpenEdges())
            {
                if ((!CanSpawnRoom(dungeon[i].GetComponent<Room>(), Direction._TOP) && dungeon[i].GetComponent<Room>().top == Gate.open) ||
                    (!CanSpawnRoom(dungeon[i].GetComponent<Room>(), Direction._RIGHT) && dungeon[i].GetComponent<Room>().right == Gate.open) ||
                    (!CanSpawnRoom(dungeon[i].GetComponent<Room>(), Direction._BOTTOM) && dungeon[i].GetComponent<Room>().bottom == Gate.open) ||
                    (!CanSpawnRoom(dungeon[i].GetComponent<Room>(), Direction._LEFT) && dungeon[i].GetComponent<Room>().left == Gate.open))
                    //dungeon[i].GetComponentInChildren<MeshRenderer>().material.SetColor("_Tint", Color.cyan);

                if (!CanSpawnRoom(dungeon[i].GetComponent<Room>(), Direction._TOP) && dungeon[i].GetComponent<Room>().top == Gate.open)
                    dungeon[i].GetComponent<Room>().top = Gate.none;
                if (!CanSpawnRoom(dungeon[i].GetComponent<Room>(), Direction._RIGHT) && dungeon[i].GetComponent<Room>().right == Gate.open)
                    dungeon[i].GetComponent<Room>().right = Gate.none;
                if (!CanSpawnRoom(dungeon[i].GetComponent<Room>(), Direction._BOTTOM) && dungeon[i].GetComponent<Room>().bottom == Gate.open)
                    dungeon[i].GetComponent<Room>().bottom = Gate.none;
                if (!CanSpawnRoom(dungeon[i].GetComponent<Room>(), Direction._LEFT) && dungeon[i].GetComponent<Room>().left == Gate.open)
                    dungeon[i].GetComponent<Room>().left = Gate.none;
            }
        }
    }
    
    void updateRooms()
    {
        for(int i=0; i<dungeon.Count; ++i)
        {
            GameObject temp=dungeon[i].GetComponent<Room>().updateRoom();
            if (dungeon[i].GetComponent<Room>().RoomID!=temp.GetComponent<Room>().RoomID)
                dungeon[i] = temp;
        }
    }
    
    void fixOpenEnds()
    {

        for (int i = 0; i < dungeon.Count; ++i)
        {
            if (dungeon[i].GetComponent<Room>() != null)
            {
                if (dungeon[i].GetComponent<Room>().OpenEdges())
                {
                    GameObject myAuxGO = dungeon[i];
                    GameObject newRoom = null;

                    //Room cases correction
                    if (myAuxGO.GetComponent<Room>().top == Gate.open)
                    {
                        //single open cases
                        if (myAuxGO.GetComponent<Room>().right == Gate.closed && myAuxGO.GetComponent<Room>().bottom == Gate.none && myAuxGO.GetComponent<Room>().left == Gate.none)
                        {
                            newRoom = Instantiate(rooms[1], myAuxGO.transform);
                            newRoom.transform.parent = null;
                        }
                        else if (myAuxGO.GetComponent<Room>().right == Gate.none && myAuxGO.GetComponent<Room>().bottom == Gate.closed && myAuxGO.GetComponent<Room>().left == Gate.none)
                        {
                            newRoom = Instantiate(rooms[2], myAuxGO.transform);
                            newRoom.transform.parent = null;
                        }
                        else if (myAuxGO.GetComponent<Room>().right == Gate.none && myAuxGO.GetComponent<Room>().bottom == Gate.none && myAuxGO.GetComponent<Room>().left == Gate.closed)
                        {
                            newRoom = Instantiate(rooms[3], myAuxGO.transform);
                            newRoom.transform.parent = null;
                        }

                        //two opens cases
                        else if (myAuxGO.GetComponent<Room>().right == Gate.closed && myAuxGO.GetComponent<Room>().bottom == Gate.closed && myAuxGO.GetComponent<Room>().left == Gate.none)
                        {
                            newRoom = Instantiate(rooms[7], myAuxGO.transform);
                            newRoom.transform.parent = null;
                        }
                        else if (myAuxGO.GetComponent<Room>().right == Gate.none && myAuxGO.GetComponent<Room>().bottom == Gate.closed && myAuxGO.GetComponent<Room>().left == Gate.closed)
                        {
                            newRoom = Instantiate(rooms[9], myAuxGO.transform);
                            newRoom.transform.parent = null;
                        }
                        else if (myAuxGO.GetComponent<Room>().right == Gate.closed && myAuxGO.GetComponent<Room>().bottom == Gate.none && myAuxGO.GetComponent<Room>().left == Gate.closed)
                        {
                            newRoom = Instantiate(rooms[8], myAuxGO.transform);
                            newRoom.transform.parent = null;
                        }

                        //three open cases
                        else if (myAuxGO.GetComponent<Room>().right == Gate.closed && myAuxGO.GetComponent<Room>().bottom == Gate.closed && myAuxGO.GetComponent<Room>().left == Gate.closed)
                        {
                            newRoom = Instantiate(rooms[13], myAuxGO.transform);
                            newRoom.transform.parent = null;
                        }

                        //cases when right is also open
                        else if (myAuxGO.GetComponent<Room>().right == Gate.open && myAuxGO.GetComponent<Room>().bottom == Gate.none && myAuxGO.GetComponent<Room>().left == Gate.closed)
                        {
                            newRoom = Instantiate(rooms[3], myAuxGO.transform);
                            newRoom.transform.parent = null;
                        }
                        else if (myAuxGO.GetComponent<Room>().right == Gate.open && myAuxGO.GetComponent<Room>().bottom == Gate.closed && myAuxGO.GetComponent<Room>().left == Gate.none)
                        {
                            newRoom = Instantiate(rooms[2], myAuxGO.transform);
                            newRoom.transform.parent = null;
                        }
                        else if (myAuxGO.GetComponent<Room>().right == Gate.open && myAuxGO.GetComponent<Room>().bottom == Gate.closed && myAuxGO.GetComponent<Room>().left == Gate.closed)
                        {
                            newRoom = Instantiate(rooms[9], myAuxGO.transform);
                            newRoom.transform.parent = null;
                        }

                        //cases when bottom is also open
                        else if (myAuxGO.GetComponent<Room>().right == Gate.none && myAuxGO.GetComponent<Room>().bottom == Gate.open && myAuxGO.GetComponent<Room>().left == Gate.closed)
                        {
                            newRoom = Instantiate(rooms[3], myAuxGO.transform);
                            newRoom.transform.parent = null;
                        }
                        else if (myAuxGO.GetComponent<Room>().right == Gate.closed && myAuxGO.GetComponent<Room>().bottom == Gate.open && myAuxGO.GetComponent<Room>().left == Gate.none)
                        {
                            newRoom = Instantiate(rooms[1], myAuxGO.transform);
                            newRoom.transform.parent = null;
                        }
                        else if (myAuxGO.GetComponent<Room>().right == Gate.closed && myAuxGO.GetComponent<Room>().bottom == Gate.open && myAuxGO.GetComponent<Room>().left == Gate.closed)
                        {
                            newRoom = Instantiate(rooms[8], myAuxGO.transform);
                            newRoom.transform.parent = null;
                        }

                        //cases when left is also open
                        else if (myAuxGO.GetComponent<Room>().right == Gate.closed && myAuxGO.GetComponent<Room>().bottom == Gate.none && myAuxGO.GetComponent<Room>().left == Gate.open)
                        {
                            newRoom = Instantiate(rooms[1], myAuxGO.transform);
                            newRoom.transform.parent = null;
                        }
                        else if (myAuxGO.GetComponent<Room>().right == Gate.none && myAuxGO.GetComponent<Room>().bottom == Gate.closed && myAuxGO.GetComponent<Room>().left == Gate.open)
                        {
                            newRoom = Instantiate(rooms[2], myAuxGO.transform);
                            newRoom.transform.parent = null;
                        }
                        else if (myAuxGO.GetComponent<Room>().right == Gate.closed && myAuxGO.GetComponent<Room>().bottom == Gate.closed && myAuxGO.GetComponent<Room>().left == Gate.open)
                        {
                            newRoom = Instantiate(rooms[7], myAuxGO.transform);
                            newRoom.transform.parent = null;
                        }

                        //right and left
                        else if (myAuxGO.GetComponent<Room>().right == Gate.open && myAuxGO.GetComponent<Room>().bottom == Gate.closed && myAuxGO.GetComponent<Room>().left == Gate.open)
                        {
                            newRoom = Instantiate(rooms[2], myAuxGO.transform);
                            newRoom.transform.parent = null;
                        }

                        //right and bottom
                        else if (myAuxGO.GetComponent<Room>().right == Gate.open && myAuxGO.GetComponent<Room>().bottom == Gate.open && myAuxGO.GetComponent<Room>().left == Gate.closed)
                        {
                            newRoom = Instantiate(rooms[3], myAuxGO.transform);
                            newRoom.transform.parent = null;
                        }

                        //bottom and left
                        else if (myAuxGO.GetComponent<Room>().right == Gate.closed && myAuxGO.GetComponent<Room>().bottom == Gate.open && myAuxGO.GetComponent<Room>().left == Gate.open)
                        {
                            newRoom = Instantiate(rooms[1], myAuxGO.transform);
                            newRoom.transform.parent = null;
                        }

                        else
                            Debug.LogError("Unhandled dungeon generation case");
                    }
                    else if (myAuxGO.GetComponent<Room>().right == Gate.open)
                    {
                        //single open cases
                        if (myAuxGO.GetComponent<Room>().top == Gate.closed && myAuxGO.GetComponent<Room>().bottom == Gate.none && myAuxGO.GetComponent<Room>().left == Gate.none)
                        {
                            newRoom = Instantiate(rooms[0], myAuxGO.transform);
                            newRoom.transform.parent = null;
                        }
                        else if (myAuxGO.GetComponent<Room>().top == Gate.none && myAuxGO.GetComponent<Room>().bottom == Gate.closed && myAuxGO.GetComponent<Room>().left == Gate.none)
                        {
                            newRoom = Instantiate(rooms[2], myAuxGO.transform);
                            newRoom.transform.parent = null;
                        }
                        else if (myAuxGO.GetComponent<Room>().top == Gate.none && myAuxGO.GetComponent<Room>().bottom == Gate.none && myAuxGO.GetComponent<Room>().left == Gate.closed)
                        {
                            newRoom = Instantiate(rooms[3], myAuxGO.transform);
                            newRoom.transform.parent = null;
                        }

                        //two opens cases
                        else if (myAuxGO.GetComponent<Room>().top == Gate.closed && myAuxGO.GetComponent<Room>().bottom == Gate.none && myAuxGO.GetComponent<Room>().left == Gate.closed)
                        {
                            newRoom = Instantiate(rooms[6], myAuxGO.transform);
                            newRoom.transform.parent = null;
                        }
                        else if (myAuxGO.GetComponent<Room>().top == Gate.none && myAuxGO.GetComponent<Room>().bottom == Gate.closed && myAuxGO.GetComponent<Room>().left == Gate.closed)
                        {
                            newRoom = Instantiate(rooms[9], myAuxGO.transform);
                            newRoom.transform.parent = null;
                        }
                        else if (myAuxGO.GetComponent<Room>().top == Gate.closed && myAuxGO.GetComponent<Room>().bottom == Gate.closed && myAuxGO.GetComponent<Room>().left == Gate.none)
                        {
                            newRoom = Instantiate(rooms[5], myAuxGO.transform);
                            newRoom.transform.parent = null;
                        }

                        //three open cases
                        else if (myAuxGO.GetComponent<Room>().top == Gate.closed && myAuxGO.GetComponent<Room>().bottom == Gate.closed && myAuxGO.GetComponent<Room>().left == Gate.closed)
                        {
                            newRoom = Instantiate(rooms[12], myAuxGO.transform);
                            newRoom.transform.parent = null;
                        }

                        //bottom is also open
                        else if (myAuxGO.GetComponent<Room>().top == Gate.closed && myAuxGO.GetComponent<Room>().bottom == Gate.open && myAuxGO.GetComponent<Room>().left == Gate.none)
                        {
                            newRoom = Instantiate(rooms[0], myAuxGO.transform);
                            newRoom.transform.parent = null;
                        }
                        else if (myAuxGO.GetComponent<Room>().top == Gate.none && myAuxGO.GetComponent<Room>().bottom == Gate.open && myAuxGO.GetComponent<Room>().left == Gate.closed)
                        {
                            newRoom = Instantiate(rooms[3], myAuxGO.transform);
                            newRoom.transform.parent = null;
                        }
                        else if (myAuxGO.GetComponent<Room>().top == Gate.closed && myAuxGO.GetComponent<Room>().bottom == Gate.open && myAuxGO.GetComponent<Room>().left == Gate.closed)
                        {
                            newRoom = Instantiate(rooms[6], myAuxGO.transform);
                            newRoom.transform.parent = null;
                        }

                        //left is also open
                        else if (myAuxGO.GetComponent<Room>().top == Gate.closed && myAuxGO.GetComponent<Room>().bottom == Gate.none && myAuxGO.GetComponent<Room>().left == Gate.open)
                        {
                            newRoom = Instantiate(rooms[0], myAuxGO.transform);
                            newRoom.transform.parent = null;
                        }
                        else if (myAuxGO.GetComponent<Room>().top == Gate.none && myAuxGO.GetComponent<Room>().bottom == Gate.closed && myAuxGO.GetComponent<Room>().left == Gate.open)
                        {
                            newRoom = Instantiate(rooms[2], myAuxGO.transform);
                            newRoom.transform.parent = null;
                        }
                        else if (myAuxGO.GetComponent<Room>().top == Gate.closed && myAuxGO.GetComponent<Room>().bottom == Gate.closed && myAuxGO.GetComponent<Room>().left == Gate.open)
                        {
                            newRoom = Instantiate(rooms[5], myAuxGO.transform);
                            newRoom.transform.parent = null;
                        }

                        //top is also open
                        else if (myAuxGO.GetComponent<Room>().top == Gate.open && myAuxGO.GetComponent<Room>().bottom == Gate.closed && myAuxGO.GetComponent<Room>().left == Gate.none)
                        {
                            newRoom = Instantiate(rooms[2], myAuxGO.transform);
                            newRoom.transform.parent = null;
                        }
                        else if (myAuxGO.GetComponent<Room>().top == Gate.open && myAuxGO.GetComponent<Room>().bottom == Gate.none && myAuxGO.GetComponent<Room>().left == Gate.closed)
                        {
                            newRoom = Instantiate(rooms[3], myAuxGO.transform);
                            newRoom.transform.parent = null;
                        }
                        else if (myAuxGO.GetComponent<Room>().top == Gate.open && myAuxGO.GetComponent<Room>().bottom == Gate.closed && myAuxGO.GetComponent<Room>().left == Gate.closed)
                        {
                            newRoom = Instantiate(rooms[9], myAuxGO.transform);
                            newRoom.transform.parent = null;
                        }

                        //bottom and left are open
                        else if (myAuxGO.GetComponent<Room>().top == Gate.closed && myAuxGO.GetComponent<Room>().bottom == Gate.open && myAuxGO.GetComponent<Room>().left == Gate.open)
                        {
                            newRoom = Instantiate(rooms[0], myAuxGO.transform);
                            newRoom.transform.parent = null;
                        }
                        //bottom and top are open
                        else if (myAuxGO.GetComponent<Room>().top == Gate.open && myAuxGO.GetComponent<Room>().bottom == Gate.open && myAuxGO.GetComponent<Room>().left == Gate.closed)
                        {
                            newRoom = Instantiate(rooms[3], myAuxGO.transform);
                            newRoom.transform.parent = null;
                        }
                        //left and top are open
                        else if (myAuxGO.GetComponent<Room>().top == Gate.open && myAuxGO.GetComponent<Room>().bottom == Gate.closed && myAuxGO.GetComponent<Room>().left == Gate.open)
                        {
                            newRoom = Instantiate(rooms[2], myAuxGO.transform);
                            newRoom.transform.parent = null;
                        }

                        else
                            Debug.LogError("Unhandled dungeon generation case");
                    }
                    else if (myAuxGO.GetComponent<Room>().bottom == Gate.open)
                    {
                        //single open cases
                        if (myAuxGO.GetComponent<Room>().top == Gate.closed && myAuxGO.GetComponent<Room>().right == Gate.none && myAuxGO.GetComponent<Room>().left == Gate.none)
                        {
                            newRoom = Instantiate(rooms[0], myAuxGO.transform);
                            newRoom.transform.parent = null;
                        }
                        else if (myAuxGO.GetComponent<Room>().top == Gate.none && myAuxGO.GetComponent<Room>().right == Gate.closed && myAuxGO.GetComponent<Room>().left == Gate.none)
                        {
                            newRoom = Instantiate(rooms[1], myAuxGO.transform);
                            newRoom.transform.parent = null;
                        }
                        else if (myAuxGO.GetComponent<Room>().top == Gate.none && myAuxGO.GetComponent<Room>().right == Gate.none && myAuxGO.GetComponent<Room>().left == Gate.closed)
                        {
                            newRoom = Instantiate(rooms[3], myAuxGO.transform);
                            newRoom.transform.parent = null;
                        }

                        //two opens cases
                        else if (myAuxGO.GetComponent<Room>().top == Gate.closed && myAuxGO.GetComponent<Room>().right == Gate.closed && myAuxGO.GetComponent<Room>().left == Gate.none)
                        {
                            newRoom = Instantiate(rooms[4], myAuxGO.transform);
                            newRoom.transform.parent = null;
                        }
                        else if (myAuxGO.GetComponent<Room>().top == Gate.closed && myAuxGO.GetComponent<Room>().right == Gate.none && myAuxGO.GetComponent<Room>().left == Gate.closed)
                        {
                            newRoom = Instantiate(rooms[6], myAuxGO.transform);
                            newRoom.transform.parent = null;
                        }
                        else if (myAuxGO.GetComponent<Room>().top == Gate.none && myAuxGO.GetComponent<Room>().right == Gate.closed && myAuxGO.GetComponent<Room>().left == Gate.closed)
                        {
                            newRoom = Instantiate(rooms[8], myAuxGO.transform);
                            newRoom.transform.parent = null;
                        }

                        //three open cases
                        else if (myAuxGO.GetComponent<Room>().top == Gate.closed && myAuxGO.GetComponent<Room>().right == Gate.closed && myAuxGO.GetComponent<Room>().left == Gate.closed)
                        {
                            newRoom = Instantiate(rooms[11], myAuxGO.transform);
                            newRoom.transform.parent = null;
                        }

                        //left is also open
                        else if (myAuxGO.GetComponent<Room>().top == Gate.closed && myAuxGO.GetComponent<Room>().right == Gate.none && myAuxGO.GetComponent<Room>().left == Gate.open)
                        {
                            newRoom = Instantiate(rooms[0], myAuxGO.transform);
                            newRoom.transform.parent = null;
                        }
                        else if (myAuxGO.GetComponent<Room>().top == Gate.none && myAuxGO.GetComponent<Room>().right == Gate.closed && myAuxGO.GetComponent<Room>().left == Gate.open)
                        {
                            newRoom = Instantiate(rooms[1], myAuxGO.transform);
                            newRoom.transform.parent = null;
                        }
                        else if (myAuxGO.GetComponent<Room>().top == Gate.closed && myAuxGO.GetComponent<Room>().right == Gate.closed && myAuxGO.GetComponent<Room>().left == Gate.open)
                        {
                            newRoom = Instantiate(rooms[4], myAuxGO.transform);
                            newRoom.transform.parent = null;
                        }

                        //right is also open
                        else if (myAuxGO.GetComponent<Room>().top == Gate.closed && myAuxGO.GetComponent<Room>().right == Gate.open && myAuxGO.GetComponent<Room>().left == Gate.none)
                        {
                            newRoom = Instantiate(rooms[0], myAuxGO.transform);
                            newRoom.transform.parent = null;
                        }
                        else if (myAuxGO.GetComponent<Room>().top == Gate.none && myAuxGO.GetComponent<Room>().right == Gate.open && myAuxGO.GetComponent<Room>().left == Gate.closed)
                        {
                            newRoom = Instantiate(rooms[3], myAuxGO.transform);
                            newRoom.transform.parent = null;
                        }
                        else if (myAuxGO.GetComponent<Room>().top == Gate.closed && myAuxGO.GetComponent<Room>().right == Gate.open && myAuxGO.GetComponent<Room>().left == Gate.open)
                        {
                            newRoom = Instantiate(rooms[6], myAuxGO.transform);
                            newRoom.transform.parent = null;
                        }

                        //top is also open
                        else if (myAuxGO.GetComponent<Room>().top == Gate.open && myAuxGO.GetComponent<Room>().right == Gate.closed && myAuxGO.GetComponent<Room>().left == Gate.none)
                        {
                            newRoom = Instantiate(rooms[1], myAuxGO.transform);
                            newRoom.transform.parent = null;
                        }
                        else if (myAuxGO.GetComponent<Room>().top == Gate.open && myAuxGO.GetComponent<Room>().right == Gate.none && myAuxGO.GetComponent<Room>().left == Gate.closed)
                        {
                            newRoom = Instantiate(rooms[3], myAuxGO.transform);
                            newRoom.transform.parent = null;
                        }
                        else if (myAuxGO.GetComponent<Room>().top == Gate.open && myAuxGO.GetComponent<Room>().right == Gate.closed && myAuxGO.GetComponent<Room>().left == Gate.closed)
                        {
                            newRoom = Instantiate(rooms[8], myAuxGO.transform);
                            newRoom.transform.parent = null;
                        }

                        //top and right open
                        else if (myAuxGO.GetComponent<Room>().top == Gate.open && myAuxGO.GetComponent<Room>().right == Gate.open && myAuxGO.GetComponent<Room>().left == Gate.closed)
                        {
                            newRoom = Instantiate(rooms[3], myAuxGO.transform);
                            newRoom.transform.parent = null;
                        }

                        //right and left open
                        else if (myAuxGO.GetComponent<Room>().top == Gate.closed && myAuxGO.GetComponent<Room>().right == Gate.open && myAuxGO.GetComponent<Room>().left == Gate.open)
                        {
                            newRoom = Instantiate(rooms[0], myAuxGO.transform);
                            newRoom.transform.parent = null;
                        }

                        //left and top open
                        else if (myAuxGO.GetComponent<Room>().top == Gate.open && myAuxGO.GetComponent<Room>().right == Gate.closed && myAuxGO.GetComponent<Room>().left == Gate.open)
                        {
                            newRoom = Instantiate(rooms[1], myAuxGO.transform);
                            newRoom.transform.parent = null;
                        }



                        else
                            Debug.LogError("Unhandled dungeon generation case");
                    }
                    else if (myAuxGO.GetComponent<Room>().left == Gate.open)
                    {
                        //single open cases
                        if (myAuxGO.GetComponent<Room>().top == Gate.closed && myAuxGO.GetComponent<Room>().right == Gate.none && myAuxGO.GetComponent<Room>().bottom == Gate.none)
                        {
                            newRoom = Instantiate(rooms[0], myAuxGO.transform);
                            newRoom.transform.parent = null;
                        }
                        else if (myAuxGO.GetComponent<Room>().top == Gate.none && myAuxGO.GetComponent<Room>().right == Gate.closed && myAuxGO.GetComponent<Room>().bottom == Gate.none)
                        {
                            newRoom = Instantiate(rooms[1], myAuxGO.transform);
                            newRoom.transform.parent = null;
                        }
                        else if (myAuxGO.GetComponent<Room>().top == Gate.none && myAuxGO.GetComponent<Room>().right == Gate.none && myAuxGO.GetComponent<Room>().bottom == Gate.closed)
                        {
                            newRoom = Instantiate(rooms[2], myAuxGO.transform);
                            newRoom.transform.parent = null;
                        }

                        //two opens cases
                        else if (myAuxGO.GetComponent<Room>().top == Gate.closed && myAuxGO.GetComponent<Room>().right == Gate.closed && myAuxGO.GetComponent<Room>().bottom == Gate.none)
                        {
                            newRoom = Instantiate(rooms[4], myAuxGO.transform);
                            newRoom.transform.parent = null;
                        }
                        else if (myAuxGO.GetComponent<Room>().top == Gate.none && myAuxGO.GetComponent<Room>().right == Gate.closed && myAuxGO.GetComponent<Room>().bottom == Gate.closed)
                        {
                            newRoom = Instantiate(rooms[7], myAuxGO.transform);
                            newRoom.transform.parent = null;
                        }
                        else if (myAuxGO.GetComponent<Room>().top == Gate.closed && myAuxGO.GetComponent<Room>().right == Gate.none && myAuxGO.GetComponent<Room>().bottom == Gate.closed)
                        {
                            newRoom = Instantiate(rooms[5], myAuxGO.transform);
                            newRoom.transform.parent = null;
                        }

                        //three open cases
                        else if (myAuxGO.GetComponent<Room>().top == Gate.closed && myAuxGO.GetComponent<Room>().right == Gate.closed && myAuxGO.GetComponent<Room>().bottom == Gate.closed)
                        {
                            newRoom = Instantiate(rooms[10], myAuxGO.transform);
                            newRoom.transform.parent = null;
                        }

                        //top is also open
                        else if (myAuxGO.GetComponent<Room>().top == Gate.open && myAuxGO.GetComponent<Room>().right == Gate.closed && myAuxGO.GetComponent<Room>().bottom == Gate.none)
                        {
                            newRoom = Instantiate(rooms[1], myAuxGO.transform);
                            newRoom.transform.parent = null;
                        }
                        else if (myAuxGO.GetComponent<Room>().top == Gate.open && myAuxGO.GetComponent<Room>().right == Gate.none && myAuxGO.GetComponent<Room>().bottom == Gate.closed)
                        {
                            newRoom = Instantiate(rooms[2], myAuxGO.transform);
                            newRoom.transform.parent = null;
                        }
                        else if (myAuxGO.GetComponent<Room>().top == Gate.open && myAuxGO.GetComponent<Room>().right == Gate.closed && myAuxGO.GetComponent<Room>().bottom == Gate.closed)
                        {
                            newRoom = Instantiate(rooms[7], myAuxGO.transform);
                            newRoom.transform.parent = null;
                        }

                        //right is also open
                        else if (myAuxGO.GetComponent<Room>().top == Gate.closed && myAuxGO.GetComponent<Room>().right == Gate.open && myAuxGO.GetComponent<Room>().bottom == Gate.none)
                        {
                            newRoom = Instantiate(rooms[0], myAuxGO.transform);
                            newRoom.transform.parent = null;
                        }
                        else if (myAuxGO.GetComponent<Room>().top == Gate.none && myAuxGO.GetComponent<Room>().right == Gate.open && myAuxGO.GetComponent<Room>().bottom == Gate.closed)
                        {
                            newRoom = Instantiate(rooms[2], myAuxGO.transform);
                            newRoom.transform.parent = null;
                        }
                        else if (myAuxGO.GetComponent<Room>().top == Gate.closed && myAuxGO.GetComponent<Room>().right == Gate.open && myAuxGO.GetComponent<Room>().bottom == Gate.closed)
                        {
                            newRoom = Instantiate(rooms[5], myAuxGO.transform);
                            newRoom.transform.parent = null;
                        }

                        //bottom is also open
                        else if (myAuxGO.GetComponent<Room>().top == Gate.closed && myAuxGO.GetComponent<Room>().right == Gate.none && myAuxGO.GetComponent<Room>().bottom == Gate.open)
                        {
                            newRoom = Instantiate(rooms[0], myAuxGO.transform);
                            newRoom.transform.parent = null;
                        }
                        else if (myAuxGO.GetComponent<Room>().top == Gate.none && myAuxGO.GetComponent<Room>().right == Gate.closed && myAuxGO.GetComponent<Room>().bottom == Gate.open)
                        {
                            newRoom = Instantiate(rooms[1], myAuxGO.transform);
                            newRoom.transform.parent = null;
                        }
                        else if (myAuxGO.GetComponent<Room>().top == Gate.closed && myAuxGO.GetComponent<Room>().right == Gate.closed && myAuxGO.GetComponent<Room>().bottom == Gate.open)
                        {
                            newRoom = Instantiate(rooms[4], myAuxGO.transform);
                            newRoom.transform.parent = null;
                        }

                        //top and right are also open
                        else if (myAuxGO.GetComponent<Room>().top == Gate.open && myAuxGO.GetComponent<Room>().right == Gate.open && myAuxGO.GetComponent<Room>().bottom == Gate.closed)
                        {
                            newRoom = Instantiate(rooms[2], myAuxGO.transform);
                            newRoom.transform.parent = null;
                        }
                        //top and bottom are also open
                        else if (myAuxGO.GetComponent<Room>().top == Gate.open && myAuxGO.GetComponent<Room>().right == Gate.closed && myAuxGO.GetComponent<Room>().bottom == Gate.open)
                        {
                            newRoom = Instantiate(rooms[1], myAuxGO.transform);
                            newRoom.transform.parent = null;
                        }
                        //right and bottom are also open
                        else if (myAuxGO.GetComponent<Room>().top == Gate.closed && myAuxGO.GetComponent<Room>().right == Gate.open && myAuxGO.GetComponent<Room>().bottom == Gate.open)
                        {
                            newRoom = Instantiate(rooms[0], myAuxGO.transform);
                            newRoom.transform.parent = null;
                        }

                        else
                            Debug.LogError("Unhandled dungeon generation case");
                    }
                    else
                        Debug.LogError("Unhandled dungeon generation case");
                    if (Application.isPlaying)
                        Destroy(dungeon[i]);
                    else if (Application.isEditor)
                        DestroyImmediate(dungeon[i]);
                    dungeon[i] = newRoom;
                    //dungeon[i].GetComponentInChildren<MeshRenderer>().material.SetColor("_Tint", Color.magenta);
                }
            }
        }
    }
    
    void fixIncoherentGates()
    {
        for(int i=0; i<dungeon.Count; ++i)
        {
            GameObject myAuxRoom = null;
            bool[] closeDirections = { false, false, false, false};

            RaycastHit[] topHits= Physics.RaycastAll(dungeon[i].transform.position, Vector3.up, roomOffset);
            RaycastHit[] rightHits= Physics.RaycastAll(dungeon[i].transform.position, Vector3.right, roomOffset);
            RaycastHit[] bottomHits= Physics.RaycastAll(dungeon[i].transform.position, Vector3.down, roomOffset);
            RaycastHit[] leftHits= Physics.RaycastAll(dungeon[i].transform.position, Vector3.left, roomOffset);

            foreach(RaycastHit rc in topHits)
            {
                if (rc.collider.gameObject != dungeon[i])
                {
                    if (dungeon[i].GetComponent<Room>().top == Gate.closed && rc.collider.gameObject.GetComponentInParent<Room>().bottom == Gate.none)
                        closeDirections[0] = true;
                }
            }
            foreach (RaycastHit rc in rightHits)
            {
                if (rc.collider.gameObject != dungeon[i])
                {
                    if (rc.collider.gameObject.GetComponentInParent<Room>() != null)
                    {
                        if (dungeon[i].GetComponent<Room>().right == Gate.closed && rc.collider.gameObject.GetComponentInParent<Room>().left == Gate.none)
                            closeDirections[1] = true;
                    }
                }
            }
            foreach (RaycastHit rc in bottomHits)
            {
                if (rc.collider.gameObject != dungeon[i])
                {
                    if (dungeon[i].GetComponent<Room>().bottom == Gate.closed && rc.collider.gameObject.GetComponentInParent<Room>().top == Gate.none)
                        closeDirections[2] = true;
                }
            }
            foreach (RaycastHit rc in leftHits)
            {
                if (rc.collider.gameObject != dungeon[i])
                {
                    if (dungeon[i].GetComponent<Room>().left == Gate.closed && rc.collider.gameObject.GetComponentInParent<Room>().right == Gate.none)
                        closeDirections[3] = true;
                }
            }

            if (closeDirections[0] || closeDirections[1] || closeDirections[2] || closeDirections[3])
            {
                
                if (closeDirections[0])
                {
                    if (closeDirections[1])
                    {
                        if (closeDirections[2])
                        {
                            myAuxRoom = Instantiate(rooms[3], dungeon[i].transform.position, Quaternion.identity);
                            //myAuxRoom.transform.parent = null;
                        }
                        else if (closeDirections[3])
                        {
                            myAuxRoom = Instantiate(rooms[2], dungeon[i].transform.position, Quaternion.identity);
                            //myAuxRoom.transform.parent = null;
                        }
                        else if (dungeon[i].GetComponent<Room>().bottom == Gate.closed && dungeon[i].GetComponent<Room>().left == Gate.none)
                        {
                            myAuxRoom = Instantiate(rooms[2], dungeon[i].transform.position, Quaternion.identity);
                            //myAuxRoom.transform.parent = null;
                        }
                        else if (dungeon[i].GetComponent<Room>().bottom == Gate.none && dungeon[i].GetComponent<Room>().left == Gate.closed)
                        {
                            myAuxRoom = Instantiate(rooms[3], dungeon[i].transform.position, Quaternion.identity);
                            //myAuxRoom.transform.parent = null;
                        }
                        else if (dungeon[i].GetComponent<Room>().bottom == Gate.closed && dungeon[i].GetComponent<Room>().left == Gate.closed)
                        {
                            myAuxRoom = Instantiate(rooms[9], dungeon[i].transform.position, Quaternion.identity);
                            //myAuxRoom.transform.parent = null;
                        }
                    }
                    else if (closeDirections[2])
                    {
                        if (closeDirections[3])
                        {
                            myAuxRoom = Instantiate(rooms[1], dungeon[i].transform.position, Quaternion.identity);
                            //myAuxRoom.transform.parent = null;
                        }
                        else if (dungeon[i].GetComponent<Room>().right == Gate.closed && dungeon[i].GetComponent<Room>().left == Gate.none)
                        {
                            myAuxRoom = Instantiate(rooms[1], dungeon[i].transform.position, Quaternion.identity);
                            //myAuxRoom.transform.parent = null;
                        }
                        else if (dungeon[i].GetComponent<Room>().right == Gate.none && dungeon[i].GetComponent<Room>().left == Gate.closed)
                        {
                            myAuxRoom = Instantiate(rooms[3], dungeon[i].transform.position, Quaternion.identity);
                            //myAuxRoom.transform.parent = null;
                        }
                        else if (dungeon[i].GetComponent<Room>().right == Gate.closed && dungeon[i].GetComponent<Room>().left == Gate.closed)
                        {
                            myAuxRoom = Instantiate(rooms[8], dungeon[i].transform.position, Quaternion.identity);
                            //myAuxRoom.transform.parent = null;
                        }

                    }
                    else if (closeDirections[3])
                    {
                        if (dungeon[i].GetComponent<Room>().right == Gate.closed && dungeon[i].GetComponent<Room>().bottom == Gate.none)
                        {
                            myAuxRoom = Instantiate(rooms[1], dungeon[i].transform.position, Quaternion.identity);
                            //myAuxRoom.transform.parent = null;
                        }
                        else if (dungeon[i].GetComponent<Room>().right == Gate.none && dungeon[i].GetComponent<Room>().bottom == Gate.closed)
                        {
                            myAuxRoom = Instantiate(rooms[2], dungeon[i].transform.position, Quaternion.identity);
                            //myAuxRoom.transform.parent = null;
                        }
                        else if (dungeon[i].GetComponent<Room>().right == Gate.closed && dungeon[i].GetComponent<Room>().bottom == Gate.closed)
                        {
                            myAuxRoom = Instantiate(rooms[7], dungeon[i].transform.position, Quaternion.identity);
                            //myAuxRoom.transform.parent = null;
                        }
                    }
                    else //one correction needed
                    {
                        //one case closed
                        if (dungeon[i].GetComponent<Room>().right == Gate.closed && dungeon[i].GetComponent<Room>().bottom == Gate.none && dungeon[i].GetComponent<Room>().left == Gate.none)
                        {
                            myAuxRoom = Instantiate(rooms[1], dungeon[i].transform.position, Quaternion.identity);
                            //myAuxRoom.transform.parent = null;
                        }
                        else if (dungeon[i].GetComponent<Room>().right == Gate.none && dungeon[i].GetComponent<Room>().bottom == Gate.closed && dungeon[i].GetComponent<Room>().left == Gate.none)
                        {
                            myAuxRoom = Instantiate(rooms[2], dungeon[i].transform.position, Quaternion.identity);
                            //myAuxRoom.transform.parent = null;
                        }
                        else if (dungeon[i].GetComponent<Room>().right == Gate.none && dungeon[i].GetComponent<Room>().bottom == Gate.none && dungeon[i].GetComponent<Room>().left == Gate.closed)
                        {
                            myAuxRoom = Instantiate(rooms[3], dungeon[i].transform.position, Quaternion.identity);
                            //myAuxRoom.transform.parent = null;
                        }

                        //two cases closed
                        else if (dungeon[i].GetComponent<Room>().right == Gate.closed && dungeon[i].GetComponent<Room>().bottom == Gate.closed && dungeon[i].GetComponent<Room>().left == Gate.none)
                        {
                            myAuxRoom = Instantiate(rooms[7], dungeon[i].transform.position, Quaternion.identity);
                            //myAuxRoom.transform.parent = null;
                        }
                        else if (dungeon[i].GetComponent<Room>().right == Gate.none && dungeon[i].GetComponent<Room>().bottom == Gate.closed && dungeon[i].GetComponent<Room>().left == Gate.closed)
                        {
                            myAuxRoom = Instantiate(rooms[9], dungeon[i].transform.position, Quaternion.identity);
                            //myAuxRoom.transform.parent = null;
                        }
                        else if (dungeon[i].GetComponent<Room>().right == Gate.closed && dungeon[i].GetComponent<Room>().bottom == Gate.none && dungeon[i].GetComponent<Room>().left == Gate.closed)
                        {
                            myAuxRoom = Instantiate(rooms[8], dungeon[i].transform.position, Quaternion.identity);
                            //myAuxRoom.transform.parent = null;
                        }

                        //three cases closed
                        else if (dungeon[i].GetComponent<Room>().right == Gate.closed && dungeon[i].GetComponent<Room>().bottom == Gate.closed && dungeon[i].GetComponent<Room>().left == Gate.closed)
                        {
                            myAuxRoom = Instantiate(rooms[13], dungeon[i].transform.position, Quaternion.identity);
                            //myAuxRoom.transform.parent = null;
                        }
                    }
                }
                else if (closeDirections[1])
                {
                    if (closeDirections[2])
                    {
                        if (closeDirections[3])
                        {
                            myAuxRoom = Instantiate(rooms[0], dungeon[i].transform.position, Quaternion.identity);
                            //myAuxRoom.transform.parent = null;
                        }
                        else
                        {
                            if (dungeon[i].GetComponent<Room>().top == Gate.closed && dungeon[i].GetComponent<Room>().left == Gate.none)
                            {
                                myAuxRoom = Instantiate(rooms[0], dungeon[i].transform.position, Quaternion.identity);
                                //myAuxRoom.transform.parent = null;
                            }
                            else if (dungeon[i].GetComponent<Room>().top == Gate.none && dungeon[i].GetComponent<Room>().left == Gate.closed)
                            {
                                myAuxRoom = Instantiate(rooms[3], dungeon[i].transform.position, Quaternion.identity);
                                //myAuxRoom.transform.parent = null;
                            }
                            else if (dungeon[i].GetComponent<Room>().top == Gate.closed && dungeon[i].GetComponent<Room>().left == Gate.closed)
                            {
                                myAuxRoom = Instantiate(rooms[6], dungeon[i].transform.position, Quaternion.identity);
                                //myAuxRoom.transform.parent = null;
                            }
                        }
                    }
                    else if (closeDirections[3])
                    {
                        if (dungeon[i].GetComponent<Room>().top == Gate.closed && dungeon[i].GetComponent<Room>().bottom == Gate.none)
                        {
                            myAuxRoom = Instantiate(rooms[0], dungeon[i].transform.position, Quaternion.identity);
                            //myAuxRoom.transform.parent = null;
                        }
                        else if (dungeon[i].GetComponent<Room>().top == Gate.none && dungeon[i].GetComponent<Room>().bottom == Gate.closed)
                        {
                            myAuxRoom = Instantiate(rooms[2], dungeon[i].transform.position, Quaternion.identity);
                            //myAuxRoom.transform.parent = null;
                        }
                        else if (dungeon[i].GetComponent<Room>().top == Gate.closed && dungeon[i].GetComponent<Room>().bottom == Gate.closed)
                        {
                            myAuxRoom = Instantiate(rooms[5], dungeon[i].transform.position, Quaternion.identity);
                            //myAuxRoom.transform.parent = null;
                        }
                    }
                    else
                    {
                        //one entry
                        if (dungeon[i].GetComponent<Room>().top == Gate.closed && dungeon[i].GetComponent<Room>().bottom == Gate.none && dungeon[i].GetComponent<Room>().left == Gate.none)
                        {
                            myAuxRoom = Instantiate(rooms[0], dungeon[i].transform.position, Quaternion.identity);
                            //myAuxRoom.transform.parent = null;
                        }
                        else if (dungeon[i].GetComponent<Room>().top == Gate.none && dungeon[i].GetComponent<Room>().bottom == Gate.closed && dungeon[i].GetComponent<Room>().left == Gate.none)
                        {
                            myAuxRoom = Instantiate(rooms[2], dungeon[i].transform.position, Quaternion.identity);
                            //myAuxRoom.transform.parent = null;
                        }
                        else if (dungeon[i].GetComponent<Room>().top == Gate.none && dungeon[i].GetComponent<Room>().bottom == Gate.none && dungeon[i].GetComponent<Room>().left == Gate.closed)
                        {
                            myAuxRoom = Instantiate(rooms[3], dungeon[i].transform.position, Quaternion.identity);
                            //myAuxRoom.transform.parent = null;
                        }

                        //two entries
                        else if (dungeon[i].GetComponent<Room>().top == Gate.closed && dungeon[i].GetComponent<Room>().bottom == Gate.none && dungeon[i].GetComponent<Room>().left == Gate.closed)
                        {
                            myAuxRoom = Instantiate(rooms[6], dungeon[i].transform.position, Quaternion.identity);
                            //myAuxRoom.transform.parent = null;
                        }
                        else if (dungeon[i].GetComponent<Room>().top == Gate.closed && dungeon[i].GetComponent<Room>().bottom == Gate.closed && dungeon[i].GetComponent<Room>().left == Gate.none)
                        {
                            myAuxRoom = Instantiate(rooms[5], dungeon[i].transform.position, Quaternion.identity);
                            //myAuxRoom.transform.parent = null;
                        }
                        else if (dungeon[i].GetComponent<Room>().top == Gate.none && dungeon[i].GetComponent<Room>().bottom == Gate.closed && dungeon[i].GetComponent<Room>().left == Gate.closed)
                        {
                            myAuxRoom = Instantiate(rooms[9], dungeon[i].transform.position, Quaternion.identity);
                            //myAuxRoom.transform.parent = null;
                        }

                        //three entries
                        else if (dungeon[i].GetComponent<Room>().top == Gate.closed && dungeon[i].GetComponent<Room>().bottom == Gate.closed && dungeon[i].GetComponent<Room>().left == Gate.closed)
                        {
                            myAuxRoom = Instantiate(rooms[12], dungeon[i].transform.position, Quaternion.identity);
                            //myAuxRoom.transform.parent = null;
                        }
                    }

                }
                else if (closeDirections[2])
                {
                    if (closeDirections[3])
                    {
                        if (dungeon[i].GetComponent<Room>().top == Gate.closed && dungeon[i].GetComponent<Room>().right == Gate.none)
                        {
                            myAuxRoom = Instantiate(rooms[0], dungeon[i].transform.position, Quaternion.identity);
                            //myAuxRoom.transform.parent = null;
                        }
                        else if (dungeon[i].GetComponent<Room>().top == Gate.none && dungeon[i].GetComponent<Room>().right == Gate.closed)
                        {
                            myAuxRoom = Instantiate(rooms[1], dungeon[i].transform.position, Quaternion.identity);
                            //myAuxRoom.transform.parent = null;
                        }
                        else if (dungeon[i].GetComponent<Room>().top == Gate.closed && dungeon[i].GetComponent<Room>().right == Gate.closed)
                        {
                            myAuxRoom = Instantiate(rooms[4], dungeon[i].transform.position, Quaternion.identity);
                            //myAuxRoom.transform.parent = null;
                        }
                    }

                    //one entry
                    else if (dungeon[i].GetComponent<Room>().right == Gate.closed && dungeon[i].GetComponent<Room>().top == Gate.none && dungeon[i].GetComponent<Room>().left == Gate.none)
                    {
                        myAuxRoom = Instantiate(rooms[1], dungeon[i].transform.position, Quaternion.identity);
                        //myAuxRoom.transform.parent = null;
                    }
                    else if (dungeon[i].GetComponent<Room>().right == Gate.none && dungeon[i].GetComponent<Room>().top == Gate.closed && dungeon[i].GetComponent<Room>().left == Gate.none)
                    {
                        myAuxRoom = Instantiate(rooms[0], dungeon[i].transform.position, Quaternion.identity);
                        //myAuxRoom.transform.parent = null;
                    }
                    else if (dungeon[i].GetComponent<Room>().right == Gate.none && dungeon[i].GetComponent<Room>().top == Gate.none && dungeon[i].GetComponent<Room>().left == Gate.closed)
                    {
                        myAuxRoom = Instantiate(rooms[3], dungeon[i].transform.position, Quaternion.identity);
                        //myAuxRoom.transform.parent = null;
                    }

                    //two entries
                    else if (dungeon[i].GetComponent<Room>().right == Gate.closed && dungeon[i].GetComponent<Room>().top == Gate.closed && dungeon[i].GetComponent<Room>().left == Gate.none)
                    {
                        myAuxRoom = Instantiate(rooms[4], dungeon[i].transform.position, Quaternion.identity);
                        //myAuxRoom.transform.parent = null;
                    }
                    else if (dungeon[i].GetComponent<Room>().right == Gate.none && dungeon[i].GetComponent<Room>().top == Gate.closed && dungeon[i].GetComponent<Room>().left == Gate.closed)
                    {
                        myAuxRoom = Instantiate(rooms[6], dungeon[i].transform.position, Quaternion.identity);
                        //myAuxRoom.transform.parent = null;
                    }
                    else if (dungeon[i].GetComponent<Room>().right == Gate.closed && dungeon[i].GetComponent<Room>().top == Gate.none && dungeon[i].GetComponent<Room>().left == Gate.closed)
                    {
                        myAuxRoom = Instantiate(rooms[8], dungeon[i].transform.position, Quaternion.identity);
                        //myAuxRoom.transform.parent = null;
                    }

                    //three entries
                    else if (dungeon[i].GetComponent<Room>().right == Gate.closed && dungeon[i].GetComponent<Room>().top == Gate.closed && dungeon[i].GetComponent<Room>().left == Gate.closed)
                    {
                        myAuxRoom = Instantiate(rooms[11], dungeon[i].transform.position, Quaternion.identity);
                        //myAuxRoom.transform.parent = null;
                    }
                }
                else if (closeDirections[3])
                {
                    //one entry
                    if (dungeon[i].GetComponent<Room>().top == Gate.closed && dungeon[i].GetComponent<Room>().right == Gate.none && dungeon[i].GetComponent<Room>().bottom == Gate.none)
                    {
                        myAuxRoom = Instantiate(rooms[0], dungeon[i].transform.position, Quaternion.identity);
                        //myAuxRoom.transform.parent = null;
                    }
                    else if (dungeon[i].GetComponent<Room>().top == Gate.none && dungeon[i].GetComponent<Room>().right == Gate.closed && dungeon[i].GetComponent<Room>().bottom == Gate.none)
                    {
                        myAuxRoom = Instantiate(rooms[1], dungeon[i].transform.position, Quaternion.identity);
                        //myAuxRoom.transform.parent = null;
                    }
                    else if (dungeon[i].GetComponent<Room>().top == Gate.none && dungeon[i].GetComponent<Room>().right == Gate.none && dungeon[i].GetComponent<Room>().bottom == Gate.closed)
                    {
                        myAuxRoom = Instantiate(rooms[2], dungeon[i].transform.position, Quaternion.identity);
                        //myAuxRoom.transform.parent = null;
                    }

                    //two entries
                    else if (dungeon[i].GetComponent<Room>().top == Gate.closed && dungeon[i].GetComponent<Room>().right == Gate.closed && dungeon[i].GetComponent<Room>().bottom == Gate.none)
                    {
                        myAuxRoom = Instantiate(rooms[4], dungeon[i].transform.position, Quaternion.identity);
                        //myAuxRoom.transform.parent = null;
                    }
                    else if (dungeon[i].GetComponent<Room>().top == Gate.none && dungeon[i].GetComponent<Room>().right == Gate.closed && dungeon[i].GetComponent<Room>().bottom == Gate.closed)
                    {
                        myAuxRoom = Instantiate(rooms[7], dungeon[i].transform.position, Quaternion.identity);
                        //myAuxRoom.transform.parent = null;
                    }
                    else if (dungeon[i].GetComponent<Room>().top == Gate.closed && dungeon[i].GetComponent<Room>().right == Gate.none && dungeon[i].GetComponent<Room>().bottom == Gate.closed)
                    {
                        myAuxRoom = Instantiate(rooms[5], dungeon[i].transform.position, Quaternion.identity);
                        //myAuxRoom.transform.parent = null;
                    }

                    //three entries
                    else if (dungeon[i].GetComponent<Room>().top == Gate.closed && dungeon[i].GetComponent<Room>().right == Gate.closed && dungeon[i].GetComponent<Room>().bottom == Gate.closed)
                    {
                        myAuxRoom = Instantiate(rooms[10], dungeon[i].transform.position, Quaternion.identity);
                        //myAuxRoom.transform.parent = null;
                    }
                }

                if (Application.isPlaying)
                    Destroy(dungeon[i]);
                else if (Application.isEditor)
                    DestroyImmediate(dungeon[i]);
                dungeon[i] = myAuxRoom;
                
                //dungeon[i].GetComponentInChildren<MeshRenderer>().material.SetColor("_Tint", Color.red);
            }
        }
    }
    
    void setExitRoom()
    {
        GameObject furthest = null;
        for(int i=0; i<dungeon.Count; ++i)
        {
            if (furthest==null || 
                (Vector3.Distance(startingRoom.transform.position, dungeon[i].transform.position) > Vector3.Distance(startingRoom.transform.position, furthest.transform.position)))
                furthest = dungeon[i];
        }

        exitRoom = furthest;
        exitRoom.GetComponent<Room>().Difficulty = Difficulty.Special;
        //exitRoom.GetComponentInChildren<MeshRenderer>().material.SetColor("_Tint", new Color(1, 0.7f, 0.7f));
    }

    void updateDungeonArr()
    {
        dungeon.Clear();
        Room[] temp=FindObjectsOfType<Room>();

        foreach(Room r in temp)
        {
            dungeon.Add(r.gameObject);
        }
    }

    private void Awake()
    {
        thresholdRoomAmount.MinValue += 2 * GameMaster.Instance.LevelNumber;
        thresholdRoomAmount.MaxValue += 2 * GameMaster.Instance.LevelNumber;
        spawnDungeon();
    }

    void Start()
    {
    }
}
