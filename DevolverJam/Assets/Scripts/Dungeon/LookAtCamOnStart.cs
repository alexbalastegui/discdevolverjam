﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookAtCamOnStart : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        transform.rotation = Quaternion.LookRotation(-Camera.main.transform.forward);
    }
}
