﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoomContent : MonoBehaviour
{
    public enum Difficulty
    {
        Easy, Medium, Hard
    }
    public enum RoomEntries
    {
        OneEntry, L_TwoEntries, TwoEntries, ThreeEntries, FourEntries
    }

    public Difficulty difficulty = Difficulty.Easy;
    public RoomEntries entries = RoomEntries.OneEntry;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
