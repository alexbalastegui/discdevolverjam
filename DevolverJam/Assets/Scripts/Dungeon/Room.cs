﻿//#define _DIFFICULTYDEBUG

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Gate
{
    open, closed, none
}

public enum Difficulty
{
    Easy, Medium, Hard, Special, None
}

public class Room : MonoBehaviour
{
    public Utils.DifficultyDebug debugDiff;

    [SerializeField]
    private Difficulty difficulty=Difficulty.None;
    public Difficulty Difficulty
    {
        get
        {
            return difficulty;
        }
        set
        {
            difficulty = value;
#if _DIFFICULTYDEBUG
            switch (difficulty)
            {
                case Difficulty.Easy:
                    GetComponentInChildren<MeshRenderer>().material.SetColor("RedChannel", Color.green);
                    break;
                case Difficulty.Medium:
                    GetComponentInChildren<MeshRenderer>().material.SetColor("RedChannel", Color.yellow);
                    break;
                case Difficulty.Hard:
                    GetComponentInChildren<MeshRenderer>().material.SetColor("RedChannel", Color.red);
                    break;
                case Difficulty.Special:
                    GetComponentInChildren<MeshRenderer>().material.SetColor("RedChannel", Color.blue);
                    break;
            }
#endif
        }
    }

    [SerializeField]
    private int roomID;
    public int RoomID
    {
        get
        {
            return roomID;
        }
    }

    [Space(10)]
    public Gate top = Gate.none;
    public Gate right = Gate.none;
    public Gate bottom = Gate.none;
    public Gate left = Gate.none;

    public bool TopWalkable
    {
        get
        {
            return (top == Gate.closed || top == Gate.open);
        }
    }
    public bool RightWalkable
    {
        get
        {
            return (right == Gate.closed || right == Gate.open);
        }
    }
    public bool BottomWalkable
    {
        get
        {
            return (bottom == Gate.closed || bottom == Gate.open);
        }
    }
    public bool LeftWalkable
    {
        get
        {
            return (left == Gate.closed || left == Gate.open);
        }
    }

    public GameObject[] rooms;

    public GameObject content=null;

    public bool OpenEdges()
    {
        return top==Gate.open || right == Gate.open || bottom == Gate.open || left == Gate.open;
    }

    private bool checkGates(bool t, bool r, bool b, bool l)
    {
        return 
            (t ? top == Gate.open || top==Gate.closed : top == Gate.none) &&
            (r ? right == Gate.open || right == Gate.closed : right == Gate.none) &&
            (b ? bottom == Gate.open || bottom == Gate.closed : bottom == Gate.none) &&
            (l ? left == Gate.open || left == Gate.closed : left == Gate.none);
    }

    [ContextMenu("Update room")]
    public GameObject updateRoom()
    {
        GameObject aux;
        //one open cases
        if(checkGates(true, false, false, false))
            aux=rooms[0];
        else if (checkGates(false, true, false, false))
            aux=rooms[1];
        else if (checkGates(false, false, true, false))
            aux=rooms[2];
        else if (checkGates(false, false, false, true))
            aux=rooms[3];

        //two open cases
        else if (checkGates(true, true, false, false))
            aux=rooms[4];
        else if (checkGates(true, false, true, false))
            aux=rooms[5];
        else if (checkGates(true, false, false, true))
            aux=rooms[6];
        else if (checkGates(false, true, true, false))
            aux=rooms[7];
        else if (checkGates(false, true, false, true))
            aux=rooms[8];
        else if (checkGates(false, false, true, true))
            aux=rooms[9];

        //three open cases
        else if (checkGates(true, true, true, false))
            aux=rooms[10];
        else if (checkGates(true, true, false, true))
            aux=rooms[11];
        else if (checkGates(true, false, true, true))
            aux=rooms[12];
        else if (checkGates(false, true, true, true))
            aux=rooms[13];

        //four open cases
        else //(checkGates(true, true, true, true))
            aux=rooms[14];

        if (aux.GetComponent<Room>().RoomID != RoomID)
        {
            aux = Instantiate(aux, transform);
            aux.transform.parent = null;
            StartCoroutine(Autodestroy(0.1f));//GameMaster.Instance.ReplaceGameObject(aux, gameObject);
        }
        return aux;
    }

    IEnumerator Autodestroy(float time)
    {
        yield return new WaitForEndOfFrame();
        if (Application.isPlaying)
            Destroy(gameObject);
        else if (Application.isEditor)
            DestroyImmediate(gameObject);
    }

    //deprecated
    public void siblingness()
    { 
        Debug.Log("Awake " + gameObject.name);
        for (int i = 0; i < 4; ++i)
        {
            Gate currentGate;
            switch (i)
            {
                default:
                case 0:
                    currentGate = top;
                    break;
                case 1:
                    currentGate = right;
                    break;
                case 2:
                    currentGate = bottom;
                    break;
                case 3:
                    currentGate = left;
                    break;
            }

            if (currentGate != Gate.none)
            {
                Vector3 dir = Vector3.zero;
                switch (i)
                {
                    default:
                    case 0:
                        dir = Vector3.up;
                        break;
                    case 1:
                        dir = Vector3.right;
                        break;
                    case 2:
                        dir = Vector3.down;
                        break;
                    case 3:
                        dir = Vector3.left;
                        break;
                }
                RaycastHit[] rch = Physics.RaycastAll(transform.position, dir, 10);

                foreach (RaycastHit r in rch)
                {
                    if (r.collider.gameObject != gameObject && r.collider.gameObject.GetComponent<Room>() != null)
                    {
                        Debug.Log(r.collider.gameObject.name + " awaken my master");
                        switch (i)
                        {
                            default:
                            case 0:
                                top = r.collider.gameObject.GetComponent<Room>().BottomWalkable ? Gate.closed : Gate.none;
                                break;
                            case 1:
                                right = r.collider.gameObject.GetComponent<Room>().LeftWalkable ? Gate.closed : Gate.none;
                                break;
                            case 2:
                                bottom = r.collider.gameObject.GetComponent<Room>().TopWalkable ? Gate.closed : Gate.none;
                                break;
                            case 3:
                                left = r.collider.gameObject.GetComponent<Room>().RightWalkable ? Gate.closed : Gate.none;
                                break;
                        }
                    }
                }
            }
        }
    }
}
