﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DungeonFiller : MonoBehaviour
{
    [System.Serializable]
    public struct RoomTypes
    {
        public List<GameObject> easy;
        public List<GameObject> medium;
        public List<GameObject> hard;
    }

    public List<GameObject> dungeonContent;

    public GameObject entryRoom;
    public GameObject exitRoom;
    public RoomTypes one_Entry;
    public RoomTypes two_Entries;
    public RoomTypes twoL_Entries;
    public RoomTypes threeEntries;
    public RoomTypes fourEntries;

    //Atempt of singleton
    public static DungeonFiller instance;
    void Awake(){
        instance = this;
    }

    private void Start()
    {
        dungeonContent = new List<GameObject>();

        //Invoke(nameof(FillDungeon), 1f);
    }

    public void Fill(){
        FillDungeon();
    }

    private void FillDungeon()
    {
        foreach(GameObject go in GetComponent<DungeonGen>().Dungeon)
        {
            //Paint all rooms red
            //foreach(MeshRenderer mr in go.GetComponentsInChildren<MeshRenderer>())
            //{
            //    mr.material.SetColor("RedChannel", Color.red);
            //}
            Room r = go.GetComponent<Room>();

            //Determine target rotation depending on
            Quaternion rotation = Quaternion.identity;
            switch (r.RoomID)
            {
                default:
                case 1:
                    rotation = Quaternion.Euler(0, 0, 0);
                    break;
                case 2:
                    rotation = Quaternion.Euler(0, 90, 0);
                    break;
                case 3:
                    rotation = Quaternion.Euler(0, 180, 0);
                    break;
                case 4:
                    rotation = Quaternion.Euler(0, 270, 0);
                    break;

                case 12:
                    rotation = Quaternion.Euler(0, 0, 0);
                    break;
                case 13:
                    rotation = Quaternion.Euler(0, 0, 0);
                    break;
                case 14:
                    rotation = Quaternion.Euler(0, 270, 0);
                    break;
                case 23:
                    rotation = Quaternion.Euler(0, 90, 0);
                    break;
                case 24:
                    rotation = Quaternion.Euler(0, 90, 0);
                    break;
                case 34:
                    rotation = Quaternion.Euler(0, 180, 0);
                    break;
                case 123:
                    rotation = Quaternion.Euler(0, 0, 0);
                    break;
                case 124:
                    rotation = Quaternion.Euler(0, 270, 0);
                    break;
                case 134:
                    rotation = Quaternion.Euler(0, 180, 0);
                    break;
                case 234:
                    rotation = Quaternion.Euler(0, 90, 0);
                    break;
                case 1234:
                    rotation = Quaternion.Euler(0, Random.Range(0, 3)*90, 0);
                    break;
            }

            int idLength = 0;
            float auxID = r.RoomID;
            while (auxID > 1)
            {
                idLength++;
                auxID /= 10;
            }

            //Determine target prefabs depending on room entries
            RoomTypes targetRoomType = new RoomTypes();
            switch (idLength)
            {
                case 0:
                case 1:
                    targetRoomType = one_Entry;
                    break;
                case 2:
                    {
                        int num1 = Mathf.FloorToInt(r.RoomID / 10);
                        int num2 = r.RoomID % 10;
                        if (Utils.isOdd(num1) ^ Utils.isOdd(num2))
                            targetRoomType = twoL_Entries;
                        else
                            targetRoomType = two_Entries;
                    }
                    break;
                case 3:
                    targetRoomType = threeEntries;
                    break;
                case 4:
                    targetRoomType = fourEntries;
                    break;
            }

            //Difficulty
            List<GameObject> targetPrefabs=null;
            switch (r.Difficulty)
            {
                case Difficulty.Easy:
                    targetPrefabs = targetRoomType.easy;
                    break;
                case Difficulty.Medium:
                    targetPrefabs = targetRoomType.medium;
                    break;
                case Difficulty.Hard:
                    targetPrefabs = targetRoomType.hard;
                    break;
                default:
                    break;
            }

            int z = 0;

            if (targetPrefabs != null && targetPrefabs.Count!=0)
            {
                r.content = Instantiate(targetPrefabs[Random.Range(0, targetPrefabs.Count-1)], r.transform.position, rotation);
                dungeonContent.Add(r.content);
            }
            else
            {
                int a = 0;
            }
        }

        if (entryRoom != null)
        {
            Room entrance = GameMaster.Instance.Dungeon.EntryRoom.GetComponent<Room>();
            entrance.content = Instantiate(entryRoom, entrance.transform.position, entrance.transform.rotation);
            dungeonContent.Add(entrance.content);
        }

        if (exitRoom != null)
        {
            Room exit = GameMaster.Instance.Dungeon.ExitRoom.GetComponent<Room>();

            Quaternion rotation = Quaternion.identity;

            switch (exit.RoomID)
            {
                default:
                case 1:
                    rotation = Quaternion.Euler(0, 0, 0);
                    break;
                case 2:
                    rotation = Quaternion.Euler(0, 90, 0);
                    break;
                case 3:
                    rotation = Quaternion.Euler(0, 180, 0);
                    break;
                case 4:
                    rotation = Quaternion.Euler(0, 270, 0);
                    break;
            }

            exit.content = Instantiate(exitRoom, exit.transform.position, rotation);
            dungeonContent.Add(exit.content);
        }
    }
}
