﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class GameMaster : MonoBehaviour
{
    private static GameMaster _instance;
    public static GameMaster Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = FindObjectOfType<GameMaster>();

                if (_instance == null)
                {
                    GameObject container = new GameObject("GameMaster");
                    _instance = container.AddComponent<GameMaster>();
                }
            }            

            return _instance;
        }
    }

    //Player
    [Header("Player")]

    public GameObject player;
    [SerializeField]
    private GameObject playerPrefab;

    //Game fields
    private float difficulty=Utils.mediumDifficultyK;
    public float Difficulty
    {
        get
        {
            return difficulty;
        }
    }

    [Header("Level")]
    //Level fields
    [SerializeField]
    private static int levelNumber = 0;
    public int LevelNumber
    {
        get
        {
            return levelNumber;
        }
    }
    private ObjectPooler pooler;
    public ObjectPooler Pooler
    {
        get
        {
            return pooler;
        }
    }

        //dungeon
    private DungeonGen dungeon;
    public DungeonGen Dungeon
    {
        get
        {
            return dungeon;
        }
    }

    public void nextLevel()
    {
        levelNumber++;
        Debug.Log("You have passed to a new level: From level " + (levelNumber - 1).ToString() + " to " + levelNumber.ToString());
        //SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex, LoadSceneMode.Single);
        Loader.Load(SceneManager.GetActiveScene().buildIndex);
        InitVars();
    }

    public void bossLevel(){
        //SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex, LoadSceneMode.Single);
        Loader.Load(3);
        InitVars();
    }

    //Utils
    public GameObject ReplaceGameObject(GameObject newGO, GameObject oldGO)
    {
        GameObject aux = Instantiate(newGO, oldGO.transform);
        aux.transform.parent = null;

        if (Application.isPlaying)
            Destroy(oldGO);
        else if (Application.isEditor)
            DestroyImmediate(oldGO);

        
        return aux;
    }

    public void ResetLevel(){
        levelNumber = 0;
    }
    
    private void Awake()
    {
        InitVars();
    }
    void InitVars()
    {

        if (_instance == null)
        {
            _instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
            Destroy(gameObject);

        //Dungeon setup
        if (FindObjectOfType<DungeonGen>() != null)
        {
            Instance.dungeon = FindObjectOfType<DungeonGen>();
        }

        //Player setup
        if (player == null && FindObjectOfType<PlayerController>() != null)
        {
            Instance.player = FindObjectOfType<PlayerController>().gameObject;
                if(Instance.dungeon!=null)
            Instance.player.transform.position = Instance.dungeon.startingRoom.transform.position+new Vector3(0, 1, 0);
        }
        else
            player = Instantiate(playerPrefab, dungeon.startingRoom.transform.parent).GetComponentInChildren<PlayerController>().gameObject;


        //Pooler
        if (FindObjectOfType<ObjectPooler>() != null)
            Instance.pooler = FindObjectOfType<ObjectPooler>();
    }

    private void Start()
    {
        LeanTween.init(800);
    }

    void dungeonDependantSetUp()
    {
        
    }
}
