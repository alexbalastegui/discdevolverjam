﻿using UnityEngine;
using System.Collections;

public enum DisablingAction
{
    Destroy, Deactivate
}

public class ParticleSystemAutoDestroy : MonoBehaviour
{
    public DisablingAction action = DisablingAction.Deactivate;
    private ParticleSystem ps;


    public void Start()
    {
        ps = GetComponent<ParticleSystem>();
    }

    public void Update()
    {
        if (ps)
        {
            if (!ps.IsAlive())
            {
                if (action == DisablingAction.Deactivate)
                    gameObject.SetActive(false);
                else
                    Destroy(gameObject);
            }
        }
    }
}