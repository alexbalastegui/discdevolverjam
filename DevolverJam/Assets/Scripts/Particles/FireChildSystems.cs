﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireChildSystems : MonoBehaviour
{
    public bool destroyOnEnd = true;
    public string soundKey = "";

    [ContextMenu("Fire Shurikens")]
    void fireChildParticles()
    {
        foreach(ParticleSystem ps in GetComponentsInChildren<ParticleSystem>())
        {
            ps.Play();            
        }
        if (soundKey.Length != 0)
            AudioManager.instance.PlayOneShot(soundKey);
    }

    private void Start()
    {
        fireChildParticles();
        Invoke(nameof(Autodestroy), 4);
    }

    void Autodestroy()
    {
        if (destroyOnEnd)
            Destroy(gameObject);
        else
            gameObject.SetActive(false);
    }
}
