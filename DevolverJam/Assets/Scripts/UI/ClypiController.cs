﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;

public class ClypiController : MonoBehaviour, IPointerDownHandler, IBeginDragHandler, IEndDragHandler, IDragHandler{

    [SerializeField] Canvas canvas;
    RectTransform rectTransform;

    Vector3 startPos;

    void Awake(){
        rectTransform = GetComponent<RectTransform>();
        startPos = transform.position;
    }

    public void OnBeginDrag(PointerEventData eventData){
        
    }

    public void OnDrag(PointerEventData eventData){
        rectTransform.anchoredPosition += eventData.delta / canvas.scaleFactor;
    }

    public void OnEndDrag(PointerEventData eventData){

    }

    public void OnPointerDown(PointerEventData eventData){

    }

    [SerializeField] Animator textFieldAnim = default;

    [Space]
    [SerializeField] TMP_Animated text = default;
    [SerializeField] Dialogue dialogue = default;
    int currentTime;
    [SerializeField, Min(0)] float moveForce = 50f;
    [SerializeField, Range(0, 1)] float moveDuration = .25f;
    bool moveClose;
    Vector2 randomDirection;

    void OnEnable(){
        transform.position = startPos;
        currentTime = 0;
        text.ClearText();
        StartCoroutine(ShowFirstText());
        textFieldAnim.SetTrigger("Open");
    }

    public void OnClick(){
        if (moveClose)
            return;

        StartCoroutine(StopClosing());
        randomDirection = Random.insideUnitCircle.normalized * moveForce;
        moveClose = true;
        currentTime++;
        if(currentTime <= dialogue.sentences.Length - 1){
            text.ReadText(dialogue.sentences[currentTime]);
        }else{
            StopCoroutine(StopClosing());
            gameObject.SetActive(false);
            moveClose = false;
        }
    }

    IEnumerator ShowFirstText(){
        yield return new WaitForSecondsRealtime(.75f);
        text.ReadText(dialogue.sentences[0]);
    }

    void Update(){
        if (moveClose){
            rectTransform.anchoredPosition += randomDirection * Time.unscaledDeltaTime;
            rectTransform.anchoredPosition = new Vector2(Mathf.Clamp(rectTransform.anchoredPosition.x, -284f, 262f), Mathf.Clamp(rectTransform.anchoredPosition.y, -126f, 246f));
        }
    }

    IEnumerator StopClosing(){
        yield return new WaitForSecondsRealtime(moveDuration);
        moveClose = false;
    }
}
