﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnlockDisk : MonoBehaviour{
    public void Unlock(){
        PlayerPrefs.SetInt("DiskUnlocked", PlayerPrefs.GetInt("DiskUnlocked", 0) + 1);
    }
}
