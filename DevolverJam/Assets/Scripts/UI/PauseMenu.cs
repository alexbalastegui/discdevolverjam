﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PauseMenu : MonoBehaviour{

    [SerializeField, Range(0, 1)] float transitionDuration = .67f;
    Animator anim = default;
    bool paused;
    bool canPause;

    Canvas canvas;

    PlayerController player;
    float percentage;
    float velocity;

    [SerializeField] Image percentageSlider = default;
    [SerializeField] TextMeshProUGUI percentageText = default;

    // Start is called before the first frame update
    void Awake(){
        canvas = GetComponent<Canvas>();
        anim = GetComponent<Animator>();
        canvas.enabled = false;
        canPause = true;
        player = FindObjectOfType<PlayerController>();
    }

    // Update is called once per frame
    void Update(){
        if (Input.GetKeyDown(KeyCode.Escape)){
            if (paused){
                StartCoroutine(DisablePause());
                anim.SetTrigger("Exit");
                canvas.enabled = false;
                paused = false;
            }else if(canPause){
                Time.timeScale = 0;
                anim.SetTrigger("Enter");
                paused = true;
                canvas.enabled = true;
                canPause = false;
            }
        }

        percentageSlider.fillAmount = Mathf.SmoothDamp(percentageSlider.fillAmount, percentage / 100, ref velocity, 1, 100, Time.unscaledDeltaTime);
    }

    public void UpdateSlider(){
        percentage = player.percentage;
        percentageText.text = percentage + "%";
        percentageSlider.fillAmount = 0;
    }

    public void Retry(){
        PlayerPrefs.DeleteKey("HP");
        PlayerPrefs.DeleteKey("Percentage");
        PlayerPrefs.DeleteKey("TimeSurvived");
        Time.timeScale = 1;
        GameMaster.Instance.ResetLevel();
        Destroy(GameMaster.Instance.gameObject);
        Loader.Load(SceneManager.GetActiveScene().buildIndex);
    }

    public void Quit(){
        GameMaster.Instance.ResetLevel();
        Destroy(GameMaster.Instance.gameObject);
        PlayerPrefs.DeleteKey("HP");
        PlayerPrefs.DeleteKey("Percentage");
        PlayerPrefs.DeleteKey("TimeSurvived");
        Time.timeScale = 1;
        Loader.Load(0);
    }

    IEnumerator DisablePause(){
        yield return new WaitForSecondsRealtime(transitionDuration);
        Time.timeScale = 1;
        canPause = true;
    }
}
