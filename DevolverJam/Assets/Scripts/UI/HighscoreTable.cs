﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class HighscoreTable : MonoBehaviour{

    [SerializeField] Transform entryContainer = default;
    [SerializeField] Transform entryTemplate = default;

    List<HighscoreEntry> highscoreEntryList;
    List<Transform> highscoreEntryTransformList;

    // Start is called before the first frame update
    void Awake(){
        entryTemplate.gameObject.SetActive(false);

        string jsonString = PlayerPrefs.GetString("HighscoreTable");
        Highscores highscores = JsonUtility.FromJson<Highscores>(jsonString);
        
        if(highscores != null){
            for (int i = 0; i < highscores.highscoreEntryList.Count; i++){
                for (int j = i + 1; j < highscores.highscoreEntryList.Count; j++){
                    if (highscores.highscoreEntryList[j].percentage > highscores.highscoreEntryList[i].percentage){
                        HighscoreEntry tmp = highscores.highscoreEntryList[i];
                        highscores.highscoreEntryList[i] = highscores.highscoreEntryList[j];
                        highscores.highscoreEntryList[j] = tmp;
                    }
                }
            }

            for (int i = 0; i < highscores.highscoreEntryList.Count; i++){
                for (int j = 0; j < highscores.highscoreEntryList.Count; j++){
                    if (j >= 5){
                        highscores.highscoreEntryList.RemoveAt(j);
                    }
                }
                if (i >= 5){
                    highscores.highscoreEntryList.RemoveAt(i);
                }
            }

            highscoreEntryTransformList = new List<Transform>();
            foreach (HighscoreEntry highscoreEntry in highscores.highscoreEntryList){
                CreateHighscoreEntryTransform(highscoreEntry, entryContainer, highscoreEntryTransformList);
            }
        }else{
            highscoreEntryList = new List<HighscoreEntry>() {
                new HighscoreEntry { boss = false, name = "AAA", percentage = 0, time = 0 },
                new HighscoreEntry { boss = false, name = "AAA", percentage = 0, time = 0 },
                new HighscoreEntry { boss = false, name = "AAA", percentage = 0, time = 0 },
                new HighscoreEntry { boss = false, name = "AAA", percentage = 0, time = 0 },
                new HighscoreEntry { boss = false, name = "AAA", percentage = 0, time = 0 }
            };

            for (int i = 0; i < highscoreEntryList.Count; i++){
                for (int j = i + 1; j < highscoreEntryList.Count; j++){
                    if (highscoreEntryList[j].percentage > highscoreEntryList[i].percentage){
                        HighscoreEntry tmp = highscoreEntryList[i];
                        highscoreEntryList[i] = highscoreEntryList[j];
                        highscoreEntryList[j] = tmp;
                    }
                }
            }

            highscoreEntryTransformList = new List<Transform>();
            foreach (HighscoreEntry highscoreEntry in highscoreEntryList){
                CreateHighscoreEntryTransform(highscoreEntry, entryContainer, highscoreEntryTransformList);
            }

            Highscores highscore = new Highscores { highscoreEntryList = highscoreEntryList };
            string json = JsonUtility.ToJson(highscore);
            PlayerPrefs.SetString("HighscoreTable", json);
            PlayerPrefs.Save();
        }
    }

    void CreateHighscoreEntryTransform(HighscoreEntry highscoreEntry, Transform container, List<Transform> transformList){
        float templateHeight = 35f;

        Transform entryTransform = Instantiate(entryTemplate, container);
        RectTransform entryRectTransform = entryTransform.GetComponent<RectTransform>();
        entryRectTransform.anchoredPosition = new Vector2(0, entryTemplate.GetComponent<RectTransform>().anchoredPosition.y - templateHeight * transformList.Count);
        entryTransform.gameObject.SetActive(true);

        #region Name
        string name = highscoreEntry.name;
        entryTransform.Find("Name").GetComponent<TextMeshProUGUI>().text = name;
        #endregion

        #region Time
        int time = highscoreEntry.time;
        float tempTime = time / 60f;
        int minutes = Mathf.FloorToInt(tempTime);
        tempTime = (tempTime - Mathf.FloorToInt(tempTime)) * 60;
        int seconds = Mathf.RoundToInt(tempTime);
        string minuteString = minutes < 10 ? "0" + minutes.ToString() : minutes.ToString();
        string secondString = seconds < 10 ? "0" + seconds.ToString() : seconds.ToString();
        entryTransform.Find("Time").GetComponent<TextMeshProUGUI>().text = minuteString + ":" + secondString;
        #endregion

        #region Percentage
        int percentage = highscoreEntry.percentage;
        entryTransform.Find("Percentage").GetComponent<TextMeshProUGUI>().text = percentage + "%";
        #endregion

        #region Boss
        bool boss = highscoreEntry.boss;
        entryTransform.Find("BossIcon").gameObject.SetActive(boss);
        #endregion

        transformList.Add(entryTransform);
    }

    void AddHighscoreEntry(int time, int percentage, string name, bool boss){
        HighscoreEntry highscoreEntry = new HighscoreEntry {name = name, percentage = percentage, time = time, boss = boss};

        string jsonString = PlayerPrefs.GetString("HighscoreTable");
        Highscores highscores = JsonUtility.FromJson<Highscores>(jsonString);

        highscores.highscoreEntryList.Add(highscoreEntry);

        string json = JsonUtility.ToJson(highscores);
        PlayerPrefs.SetString("HighscoreTable", json);
        PlayerPrefs.Save();
    }

    class Highscores{
        public List<HighscoreEntry> highscoreEntryList;
    }

    [System.Serializable]
    class HighscoreEntry{
        public bool boss;
        public int time;
        public int percentage;
        public string name;
    }
}
