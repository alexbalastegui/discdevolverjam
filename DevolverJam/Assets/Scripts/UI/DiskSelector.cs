﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DiskSelector : MonoBehaviour{

    [SerializeField] int skinSelected = 0;
    [SerializeField] bool[] skinsUnlocked = new bool[5];
    [SerializeField] Button[] disks = new Button[5];

    // Start is called before the first frame update
    void Awake(){
        if (!PlayerPrefs.HasKey("Disk"))
            PlayerPrefs.SetInt("Disk", 0);

        for (int i = 0; i < skinsUnlocked.Length; i++){
            if(PlayerPrefs.GetInt("DiskUnlocked", 0) >= i){
                skinsUnlocked[i] = true;
            }
            if (!skinsUnlocked[i]){
                disks[i].interactable = false;
            }
        }
    }

    public void SelectDisk(int number){
        PlayerPrefs.SetInt("Disk", number);
    }
}
