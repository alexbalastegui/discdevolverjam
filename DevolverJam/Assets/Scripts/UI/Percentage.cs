﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Percentage : MonoBehaviour{

    PlayerController player;
    float percentage;
    float velocity;

    [SerializeField] Image percentageSlider = default;
    [SerializeField] TextMeshProUGUI percentageText = default;

    [SerializeField] TextMeshProUGUI timeText = default;

    void Awake(){
        player = FindObjectOfType<PlayerController>();
    }

    // Update is called once per frame
    void Update(){
        percentageSlider.fillAmount = Mathf.SmoothDamp(percentageSlider.fillAmount, percentage / 100, ref velocity, 1, 100, Time.unscaledDeltaTime);
    }

    public void UpdateSlider(){
        percentage = player.percentage;
        percentageText.text = percentage + "%";
        percentageSlider.fillAmount = 0;

        if(timeText != null){
            int time = Mathf.RoundToInt(player.timeSurvived);
            float tempTime = time / 60f;
            int minutes = Mathf.FloorToInt(tempTime);
            tempTime = (tempTime - Mathf.FloorToInt(tempTime)) * 60;
            int seconds = Mathf.RoundToInt(tempTime);
            string minuteString = minutes < 10 ? "0" + minutes.ToString() : minutes.ToString();
            string secondString = seconds < 10 ? "0" + seconds.ToString() : seconds.ToString();
            timeText.text = "time: " + minuteString + ":" + secondString;
        }
    }
}
