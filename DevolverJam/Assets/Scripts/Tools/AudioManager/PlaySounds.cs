﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaySounds : MonoBehaviour{

    public void PlayError(){
        AudioManager.instance.PlayOneShot("critical_error", true);
    }
}
