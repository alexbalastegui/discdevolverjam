﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class ScreenShakeCall : MonoBehaviour{

    CinemachineVirtualCamera vCam;
    float shakeTimer;
    float shakeTimerTotal;
    float startingIntensity;

    public static ScreenShakeCall instance { get; private set; }

    void Awake(){
        instance = this;
        vCam = GetComponent<CinemachineVirtualCamera>();
    }

    /// <summary> Smoothly shakes with a certain intensity and duration </summary>
    public void ShakeCamera(float intensity, float time){
        startingIntensity = intensity;
        shakeTimer = shakeTimerTotal = time;
    }

    public void StopShaking(){
        shakeTimer = 0;
        CinemachineBasicMultiChannelPerlin multiChannelPerlin = vCam.GetCinemachineComponent<CinemachineBasicMultiChannelPerlin>();
        multiChannelPerlin.m_AmplitudeGain = 0;
    }

    void Update(){
        if (shakeTimer > 0){
            shakeTimer -= Time.deltaTime;
            CinemachineBasicMultiChannelPerlin multiChannelPerlin = vCam.GetCinemachineComponent<CinemachineBasicMultiChannelPerlin>();
            multiChannelPerlin.m_AmplitudeGain = Mathf.Lerp(startingIntensity, 0f, 1 - (shakeTimer / shakeTimerTotal));
        }
    }
}
