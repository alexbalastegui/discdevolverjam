﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.UI;
using TMPro;
using UnityEngine.Audio;
using UnityEngine.Events;

[System.Serializable] public class OnPlay : UnityEvent { }

public class MenuController : MonoBehaviour{

    public OnPlay onPlay;

    [SerializeField] AudioMixer mainMixer = default;
    [Tooltip("The music volume the first time you start the game")] [SerializeField, Range(0, 1)] float defaultMusicValue = 1f;
    [Tooltip("The SFX volume the first time you start the game")] [SerializeField, Range(0, 1)] float defaultSfxValue = 1f;
    [SerializeField] Slider musicSlider = default;
    [SerializeField] Slider sfxSlider = default;

    [Space]
    [SerializeField] TMP_Dropdown qualityDropdown = default;
    int qualitySelected;

    [Space]
    [SerializeField] TMP_Dropdown resolutionDropdown = default;
    Resolution[] resolutions;
    int currentResolutionIndex;

    void Awake(){
        if (resolutionDropdown != null){
            resolutions = Screen.resolutions.Select(resolution => new Resolution { width = resolution.width, height = resolution.height }).Distinct().ToArray();
            resolutionDropdown.ClearOptions();

            List<string> options = new List<string>();
            for (int i = 0; i < resolutions.Length; i++){
                string option = resolutions[i].width + "x" + resolutions[i].height;
                options.Add(option);

                if (resolutions[i].width == Screen.currentResolution.width && resolutions[i].height == Screen.currentResolution.height){
                    currentResolutionIndex = i;
                }
            }

            resolutions.Reverse();

            resolutionDropdown.AddOptions(options);
            resolutionDropdown.value = currentResolutionIndex;
            resolutionDropdown.RefreshShownValue();
        }

        if(qualityDropdown != null){
            if (PlayerPrefs.HasKey("QualitySelected")){
                qualitySelected = PlayerPrefs.GetInt("QualitySelected");
            }else{
                qualitySelected = qualityDropdown.value;
            }

            qualityDropdown.value = qualitySelected;
            QualitySettings.SetQualityLevel(qualitySelected);
        }
    }

    void Start(){
        Time.timeScale = 1;
        if (musicSlider != null && sfxSlider != null){
            musicSlider.value = PlayerPrefs.GetFloat("MusicVolume", defaultMusicValue);
            sfxSlider.value = PlayerPrefs.GetFloat("SFXVolume", defaultSfxValue);
            mainMixer.SetFloat("SFX", Mathf.Log10(PlayerPrefs.GetFloat("SFXVolume", defaultSfxValue)) * 20);
            mainMixer.SetFloat("Master", Mathf.Log10(PlayerPrefs.GetFloat("MusicVolume", defaultMusicValue)) * 20);
        }

        AudioManager.instance.StopAll();
        AudioManager.instance.Play("MenuIntro");
        AudioManager.instance.Play("MenuMain", 2.1f);
    }

    //Needs a slider between 0.0001 and 1
    public void SetMusicVolume(float sliderValue){
        mainMixer.SetFloat("Master", Mathf.Log10(sliderValue) * 20);
        PlayerPrefs.SetFloat("MusicVolume", sliderValue);
    }

    //Needs a slider between 0.0001 and 1
    public void SetSfxVolume(float sliderValue){
        mainMixer.SetFloat("SFX", Mathf.Log10(sliderValue) * 20);
        PlayerPrefs.SetFloat("SFXVolume", sliderValue);
    }

    public void SetQuality(int qualityIndex){
        QualitySettings.SetQualityLevel(qualityIndex);
        PlayerPrefs.SetInt("QualitySelected", qualityIndex);
    }

    public void SetResolution(int resolutionIndex){
        Resolution resolution = resolutions[resolutionIndex];
        Screen.SetResolution(resolution.width, resolution.height, Screen.fullScreen);
    }

    public void Play(){
        PlayerPrefs.DeleteKey("HP");
        PlayerPrefs.DeleteKey("Percentage");
        PlayerPrefs.DeleteKey("TimeSurvived");
        onPlay.Invoke();
    }

    public void Quit(){
        Application.Quit();
    }
}
