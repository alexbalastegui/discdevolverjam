﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Portal : MonoBehaviour
{
    public bool bossPortal = false;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            if (bossPortal){
                GameMaster.Instance.bossLevel();
                return;
            }

            //Añadir cortinita y delay si hace falta
            GameMaster.Instance.nextLevel();
        }
    }
}
