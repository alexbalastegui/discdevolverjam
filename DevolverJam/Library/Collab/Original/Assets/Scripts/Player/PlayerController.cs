﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour{

    [HideInInspector] public bool bossKilled;
    [HideInInspector] public float timeSurvived;

    [SerializeField] Transform container = default;
    Rigidbody rb;
    [SerializeField] float speed = 6f;
    [SerializeField] LayerMask whatIsGround = -1;
    [SerializeField] Animator anim = default;
    float rotationVelocity;

    [Space]
    public int weaponSelected;
    [SerializeField] Transform shootPos = default;

    [Space]
    [SerializeField] Transform aimPos = default;
    PlayerHealth health;
    Camera mainCamera;

    [SerializeField, Range(0, .5f)] float timeBetweenSteps = .1f;
    float currentStepTime;
    bool step;

    // Start is called before the first frame update
    void Awake(){
        rb = GetComponent<Rigidbody>();
        weaponSelected = 1;
        health = FindObjectOfType<PlayerHealth>();
        mainCamera = Camera.main;
    }

    // Update is called once per frame
    void Update(){
        Vector3 movementInput = new Vector3(Input.GetAxisRaw("Horizontal"), 0, Input.GetAxisRaw("Vertical")).normalized;

        Ray ray = mainCamera.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

        if(Physics.Raycast(ray, out hit, 1000f, whatIsGround) && Time.timeScale != 0 && Input.GetMouseButtonDown(0) && health && health.currentHp > 0){
            Vector3 pos = hit.point - transform.position;
            float rotY = Mathf.Atan2(-pos.z, pos.x) * Mathf.Rad2Deg;
            transform.rotation = Quaternion.Euler(0f, rotY + 90, 0f);
        }

        if (Input.GetMouseButtonDown(0) && Time.timeScale != 0 && health && health.currentHp > 0){
            FrisbieController frisbie = ObjectPooler.instance.SpawnFromPool("Disk", new Vector3(shootPos.position.x, .3f, shootPos.position.z), transform.rotation).GetComponent<FrisbieController>();
            //FrisbieController frisbie = Instantiate(disk, transform.position, transform.rotation).GetComponent<FrisbieController>();
            AudioManager.instance.PlayOneShot("laser" + Random.Range(1, 6));
            switch (weaponSelected){
                case 1:
                    frisbie.frisbieType = FrisbieController.FrisbieType.Default;
                    frisbie.AddForce(movementInput * speed);
                    break;
                case 2:
                    frisbie.frisbieType = FrisbieController.FrisbieType.Boomerang;
                    frisbie.AddForce(movementInput * speed);
                    break;
                case 3:
                    frisbie.frisbieType = FrisbieController.FrisbieType.Explosive;
                    frisbie.AddForce(movementInput * speed);
                    break;
            }
            health.RemoveDisk();
        }
    }

    void FixedUpdate(){
        Vector3 movementInput = new Vector3(Input.GetAxisRaw("Horizontal"), 0, Input.GetAxisRaw("Vertical")).normalized;
        anim.SetFloat("Speed", movementInput.magnitude);
        if (movementInput.magnitude >= .1f && !Input.GetMouseButtonDown(0)){
            float targetAngle = Mathf.Atan2(movementInput.x, movementInput.z) * Mathf.Rad2Deg + mainCamera.transform.eulerAngles.y;
            float angle = Mathf.SmoothDampAngle(transform.eulerAngles.y, targetAngle, ref rotationVelocity, .1f);
            transform.rotation = Quaternion.Euler(0, angle, 0);

            Vector3 moveDirection = Quaternion.Euler(0, targetAngle, 0) * Vector3.forward;
            //rb.MovePosition(rb.position + moveDirection.normalized * speed * Time.deltaTime);
            rb.velocity = moveDirection.normalized * speed;

            currentStepTime -= Time.deltaTime;
            if(currentStepTime <= 0 && AudioManager.instance!=null){
                if (step){
                    AudioManager.instance.PlayOneShot("paso1");
                    step = false;
                }else{
                    AudioManager.instance.PlayOneShot("paso2");
                    step = true;
                }
                currentStepTime = timeBetweenSteps;
            }
        }else{
            rb.velocity = Vector3.zero;
        }
    }

    void LateUpdate(){
        Ray ray = mainCamera.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit, 1000f, whatIsGround) && Time.timeScale != 0){
            aimPos.position = new Vector3(hit.point.x, aimPos.position.y, hit.point.z);
        }
    }

    public void Damage(int amt){
        for (int i = 0; i < amt; i++){
            if(health!=null)
                health.RemoveDisk();
        }
    }

    void OnTriggerEnter(Collider col){
        if (col.CompareTag("Disk")){
            health.AddDisk();
            col.transform.parent.gameObject.SetActive(false);
        }
    }
}
