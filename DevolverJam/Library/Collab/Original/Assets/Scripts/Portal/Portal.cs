﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Portal : MonoBehaviour
{
    public bool bossPortal = false;

    public Material redPortal;
    public Material greenPortal;
    public GameObject particleDots;
    public GameObject bits;

    private void Start()
    {
        if (bossPortal)
        {
            GetComponentInChildren<MeshRenderer>().material = redPortal;
            particleDots.GetComponent<ParticleSystemRenderer>().material.SetColor("_Color", Color.red);
            bits.GetComponent<ParticleSystem>().startColor = Color.red;
        }
        else
        {
            GetComponentInChildren<MeshRenderer>().material = greenPortal;
            particleDots.GetComponent<ParticleSystemRenderer>().material.SetColor("_Color", Color.green);
            bits.GetComponent<ParticleSystem>().startColor = Color.green;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            if (bossPortal){
                GameMaster.Instance.bossLevel();
                return;
            }

            //Añadir cortinita y delay si hace falta
            GameMaster.Instance.nextLevel();
        }
    }
}
