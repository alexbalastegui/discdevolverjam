﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Gate
{
    open, closed, none
}

public class Room : MonoBehaviour
{
    public Gate top = Gate.none;
    public Gate right = Gate.none;
    public Gate bottom = Gate.none;
    public Gate left = Gate.none;

    public bool OpenEdges()
    {
        return top==Gate.open || right == Gate.open || bottom == Gate.open || left == Gate.open;
    }
}
