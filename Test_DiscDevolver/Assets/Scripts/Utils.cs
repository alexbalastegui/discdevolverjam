﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Direction
{
    _TOP, _RIGHT, _BOTTOM, _LEFT, _MAX
}

public static class Utils
{
    public static Vector3 dirToVec(Direction direction)
    {
        switch (direction)
        {
            case Direction._TOP:
                return new Vector3(0, 0, 1);
            case Direction._RIGHT:
                return new Vector3(1, 0, 0);
            case Direction._BOTTOM:
                return new Vector3(0, 0, -1);
            case Direction._LEFT:
                return new Vector3(-1, 0, 0);
        }
        return Vector3.one;
    }

}